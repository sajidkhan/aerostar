# Aerostar
Aerostar is a SaaS transport scheduling application tailored for Agencies that aim to empower people with developmental disabilities through community based services. The Agencies will drive the people to a Location where they can help them learn a specific skill.

## Project Requirements
- Python 3.6
- .env file from zoho

## Set up
* [Development Set Up](docs/development.md)
* [Architecture](docs/architecture.md)
* [Libraries Used](docs/patterns/)
* [How to Debug](docs/debugging.md)

## Deployment
* [Deployment Instructions](docs/deployment.md)

# URLs

* Development - https://app.dev.aerostarscheduler.com/
* Production - https://app.aerostarscheduler.com/
* Django Admin - https://api.aerostarscheduler.com/api/admin/
* DRF - https://api.aerostarscheduler.com/api/v1/aerostar/

