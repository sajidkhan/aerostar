from django.db import models
from django.db.models.signals import post_save
from simple_history.models import HistoricalRecords

from aerostar.models import BaseModel, Agency, User, Location, Skill, Program
from transportation.models import TransportationType
from aerostar.models import Address
from django.dispatch import receiver


class EventType(BaseModel):
    history = HistoricalRecords()
    PICKUP = 1
    CHECK_IN = 2
    ARRIVE = 3
    CALLED_OUT = 4
    NO_SHOW = 5
    DEPART = 7
    DONE = 8
    SCHEDULED = 9
    SCHEDULED_OFF = 10
    UNSCHEDULED = 11
    DECLINED_SERVICES = 12
    NONE = 999
    # EVENTTYPES = (
    #     ('pickup', 'pickup'),
    #     ('check_in', 'check_in'),
    #     ('arrive', 'arrive'),
    #     ('depart', 'depart'),
    #     ('called_out', 'called_out'),
    #     ('no_show', 'no_show'),
    #     ('done', 'done')
    # )
    event = models.CharField(max_length=30)
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name = 'EventType'
        verbose_name_plural = 'EventTypes'

    def __str__(self):
        return f'{self.id}: {self.event}'


class ScheduleType(BaseModel):
    history = HistoricalRecords()
    type = models.CharField(max_length=30, default='start')
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE)
    program = models.ForeignKey(Program, on_delete=models.DO_NOTHING, blank=True, null=True)
    start_time = models.TimeField()
    end_time = models.TimeField()
    require_survey = models.BooleanField(blank=True, default=False)

    class Meta:
        verbose_name = 'ScheduleType'
        verbose_name_plural = 'ScheduleTypes'

    def __str__(self):
        return f'{self.id}: {self.type}'

class ScheduleTypeEventTypeAction(BaseModel):
    event_type = models.ForeignKey(EventType, on_delete=models.CASCADE)
    schedule_type = models.ForeignKey(ScheduleType, on_delete=models.DO_NOTHING)
    sort_order = models.CharField(max_length=3)

    class Meta:
        verbose_name = 'ScheduleTypeEventTypeAction'
        verbose_name_plural = 'ScheduleTypeEventTypeActions'

    def __str__(self):
        return f'{self.id}: {self.event_type} {self.schedule_type}'
        
class Schedule(BaseModel):
    history = HistoricalRecords()
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE)
    date = models.DateField()
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='schedule_created_by_user')
    schedule_type = models.ForeignKey(ScheduleType, on_delete=models.DO_NOTHING)
    is_master = models.BooleanField(default=False)
    approved_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True,
                                    related_name='approved_by_user')
    program = models.ForeignKey(Program, on_delete=models.DO_NOTHING, blank=True, null=True)
    is_modified = models.BooleanField(default=False)


    def is_approved(self):
        return self.approved_by is not None

    class Meta:
        verbose_name = 'Schedule'
        verbose_name_plural = 'Schedules'
        constraints = [
            models.UniqueConstraint(fields=['agency', 'date', 'schedule_type', 'program'], name='schedule_constraint')
        ]

    def __str__(self):
        return f'{self.id}: {self.created_by}'


class EventLog(BaseModel):
    STATUS_MAP = {
        'scheduled': EventType.SCHEDULED,
        'scheduled_off': EventType.SCHEDULED_OFF,
        'unscheduled': EventType.UNSCHEDULED,
        'called_out': EventType.CALLED_OUT,
        'no_show': EventType.NO_SHOW,
        'off': EventType.SCHEDULED_OFF,
        'declined_services': EventType.DECLINED_SERVICES,
    }
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='created_by_user')
    event_on_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='event_on_user')
    event_type = models.ForeignKey(EventType, on_delete=models.DO_NOTHING, null=True, blank=True )
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE, null=True, blank=True)
    comment = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = 'EventLog'
        verbose_name_plural = 'EventLogs'

    def __str__(self):
        return f'{self.id}: {self.created_by} {self.event_on_user}'


class ScheduledCoach(BaseModel):
    history = HistoricalRecords()
    STATUSTYPES = (
        ('scheduled', 'scheduled'),
        ('scheduled_off', 'scheduled off'),
        ('unscheduled', 'unscheduled'),
        ('called_out', 'called out'),
    )
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE)
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    location_start = models.ForeignKey(Location, on_delete=models.CASCADE, blank=True, null=True, related_name="location_start")
    location_end = models.ForeignKey(Location, on_delete=models.CASCADE, blank=True, null=True, related_name="location_end")
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE, blank=True, null=True)
    transportation_type = models.ForeignKey(TransportationType, on_delete=models.CASCADE, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=30, choices=STATUSTYPES, default='scheduled')

    class Meta:
        verbose_name = 'ScheduledCoach'
        verbose_name_plural = 'ScheduledCoaches'
        constraints = [
            models.UniqueConstraint(fields=['user', 'schedule'], name='scheduled_coach_constraint')
        ]

    def __str__(self):
        return f'{self.id}: {self.schedule} {self.user}'


class ScheduledParticipant(BaseModel):
    history = HistoricalRecords()
    STATUSTYPES = (
        ('scheduled', 'scheduled'),
        ('off', 'off'),
        ('called_out', 'called out'),
        ('no_show', 'no_show'),
        ('declined_services', 'declined_services'),
    )
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE)
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    scheduled_coach = models.ForeignKey(ScheduledCoach, on_delete=models.CASCADE, related_name='scheduled_participants', blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    # Needs Main Office Drop Off
    requires_mo_drop_off = models.BooleanField(default=False)
    note = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=30, choices=STATUSTYPES, default='scheduled')
    skill = models.ForeignKey(Skill, on_delete=models.DO_NOTHING, blank=True, null=True)

    class Meta:
        verbose_name = 'ScheduledParticipant'
        verbose_name_plural = 'ScheduledParticipants'
        constraints = [
            models.UniqueConstraint(fields=['schedule', 'user'], name='scheduled_participant_constraint')
        ]

    def __str__(self):
        return f'{self.id}: {self.scheduled_coach} {self.user}'

    def last_event_log(self):
        try:
            return EventLog.objects.get(agency=self.agency, schedule=self.scheduled_coach.schedule,
                                        coach=self.scheduled_coach.user, event_on_user=self.user)
        except EventLog.DoesNotExist:
            return None


@receiver(post_save, sender=ScheduledParticipant)
def add_scheduled_coach_transportation_type(sender, **kwargs):
    scheduled_coach = kwargs['instance'].scheduled_coach
    user = kwargs['instance'].user
    if scheduled_coach and user and scheduled_coach.transportation_type is None:
        scheduled_coach.transportation_type = user.transportation_type
        scheduled_coach.save()

