from datetime import date, datetime, timedelta
import itertools
from django.db import transaction
from django.db.models import F, Prefetch
from django.db.models.functions import Lower
from rest_framework import viewsets, status
from aerostar.models import UserType, User, Program
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist

from schedules.copy_from_master import CopyFromMaster
from schedules.models import EventType, ScheduleType, Schedule, EventLog, ScheduledCoach, ScheduledParticipant, ScheduleTypeEventTypeAction
from schedules.serializers import EventTypeSerializer, ScheduleTypeSerializer, ScheduleSerializer, EventLogSerializer, \
    ScheduledCoachSerializer, ScheduledParticipantSerializer, ScheduleByAgencyAndDateSerializer, ScheduleTypeEventTypeActionSerializer
from rest_framework.decorators import APIView, api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from django.http import JsonResponse
from rest_flex_fields import FlexFieldsModelViewSet
from aerostar.permissions import UserPermissionFactory, UserPermission
from functools import partial
from django.db.models import Q
from rest_framework.decorators import action
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
import sys


def send_email_to_coach(email, msg):
    if settings.EMAIL_NOTIFICATIONS is True:
        subject, from_email, to = 'Schedule Change', settings.DEFAULT_FROM_EMAIL, email
        text_content = msg
        html_content = msg
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()


class EventTypeViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER], ['GET', 'HEAD', 'OPTIONS']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
    ]),)   
    serializer_class = EventTypeSerializer
    queryset = EventType.objects.all()

    def get_queryset(self, *args, **kwargs):
        return EventType.objects.filter(Q(agency_id=self.request.user.agency_id) | Q(agency_id__isnull=True))


class ScheduleTypeViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER],
                       ['GET', 'HEAD', 'OPTIONS']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),             
    ]),)
    serializer_class = ScheduleTypeSerializer
    queryset = ScheduleType.all_objects.all()

    def get_queryset(self, *args, **kwargs):
        return ScheduleType.all_objects.filter(agency_id=self.request.user.agency_id)

    def create(self, request, *args, **kwargs):
        event_types = request.data.get('event_types')
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        Schedule_type = serializer.save()
        Schedule_type.save()

        # Check if any event types included in data. if so add event types to scheduleeventtypeactions model.
        for event in event_types:
            ScheduleTypeEventTypeAction.objects.create(
                schedule_type=Schedule_type,
                event_type=EventType.objects.get(id = event['id']), 
                sort_order = event['sort_order']
            )
        return Response(serializer.data, status=201)    

    def partial_update(self, request, *args, **kwargs):
        event_types = request.data.get('event_types')
        instance = self.queryset.get(pk=kwargs.get('pk'))
        serializer = self.serializer_class(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        schedule_type = serializer.save()
        db_event_list = ScheduleTypeEventTypeAction.objects.filter(schedule_type=schedule_type).order_by('sort_order').select_related('event_type') 
        db_event_list.delete()

        for event in event_types:
            ScheduleTypeEventTypeAction.objects.create(
                schedule_type=schedule_type,
                event_type=EventType.objects.get(id = event['key']), 
                sort_order = event['sort_order']
            )      

        return Response(serializer.data, status=201)


class ScheduleByDateAPIView(APIView):
    permission_classes = [IsAuthenticated]
    def add_missing_coaches(self, schedule_by_date, program):
        # Return all coaches within the selected program.  Add them to the current day scheduled table if they are missing
        current_coaches = ScheduledCoach.all_objects.filter(schedule=schedule_by_date, user__program=program).select_related('user')
        missing_coaches = User.objects.filter(agency_id=self.request.user.agency_id,
                                              user_type__in=[UserType.COACH, UserType.TRANSPORTER], program=program).exclude(
            pk__in=[o.user.id for o in current_coaches])

        for missing_coach in missing_coaches:
            added_coach = ScheduledCoach(schedule=schedule_by_date,
                                         user=missing_coach,
                                         agency_id=self.request.user.agency_id)
            added_coach.save()

    def get_missing_participants(self, schedule_by_date, program, current_participants):

        # Return all participants.  Add them to the scheduled table if they are missing

        # ScheduledParticipant are SoftDeleted and the SafeDeleteModel QuerySetManager automatically filters those records out.
        # When we calculate the "missing_participant", the SoftDeleted records show up as missing.
        # Since the record is still present, when we try to add it again, we get a Duplicate Key Constraint failure on line #78
        #
        # Still looking into HOW those records are getting Soft Deleted, but as a temp fix, we use `.all_objects.`  
        # to make sure ALL of the records are getting returned, soft deleted or not.
        missing_participants = User.objects.filter(agency_id=self.request.user.agency_id,
                                                   user_type=UserType.PARTICIPANT, program=program).exclude(
            pk__in=[o.user.id for o in current_participants])

        added_participants = []
        for missing_participant in missing_participants:
            added_participant = ScheduledParticipant(schedule=schedule_by_date,
                                                     user=missing_participant,
                                                     agency_id=self.request.user.agency_id)
            added_participants.append(added_participant)

        return added_participants

    def copying_from_master(self, schedule_date, program, schedule_type, user):
        """
        Copy the Daily schedule from Master if it doesn't exist and return it.
        If the schedule exists, return it.
        """
        schedule_by_date, created = Schedule.objects.get_or_create(
            date=schedule_date,
            program_id=program,
            agency_id=self.request.user.agency_id,
            schedule_type=schedule_type, defaults={'created_by': user})
        self.add_missing_coaches(schedule_by_date, program)
        if not schedule_by_date.is_modified:
            current_participants = ScheduledParticipant.all_objects.filter(
                schedule=schedule_by_date).select_related(
                'user')
            unscheduled_participants = self.get_missing_participants(schedule_by_date, program, current_participants)
            
            CopyFromMaster(self.request, program, schedule_by_date, schedule_date, schedule_type,
                            itertools.chain(current_participants, unscheduled_participants))
        return schedule_by_date

    def _validateRequest(self, request):
        """
        Handle any discrepancies in the request data and return valid data to be used.
        """
        schedule_date = request.data.get('date')
        program_id = request.data.get('program')
        schedule_type_id = request.data.get('schedule_type_id')

        if schedule_date is None:
            return JsonResponse({'error': 'POST must contain a date'}, status=status.HTTP_400_BAD_REQUEST)

        if not program_id:
            if request.user.user_type.id == UserType.ADMIN:
                program_list = Program.objects.filter(agency=request.user.agency).exclude(deleted__isnull=False).order_by('id')
                program_id = program_list[0].id
            else:
                program_list = Program.objects.filter(users__in=[request.user.id]).exclude(deleted__isnull=False).order_by('id')
                program_id = program_list[0].id
        else:
            program_check = Program.all_objects.filter(agency=request.user.agency, id=program_id)
            if program_check[0].deleted:
                if request.user.user_type.id == UserType.ADMIN:
                    program_list = Program.objects.filter(agency=request.user.agency).exclude(deleted__isnull=False).order_by('id')
                    program_id = program_list[0].id
                else:
                    program_list = Program.objects.filter(users__in=[request.user.id]).exclude(deleted__isnull=False).order_by('id')
                    program_id = program_list[0].id

        schedule_types = ScheduleType.objects.filter(agency_id=self.request.user.agency_id, program_id=program_id).order_by('start_time')
        if len(schedule_types) == 0:
            return JsonResponse({'error': 'No schedule type assigned to current program'}, status=status.HTTP_400_BAD_REQUEST)

        # When we switch Programs, the front end sends the old `schedule_type`
        # (TODO: update front end code, not to do this??)
        # so we need to add an extra check to make sure that they belong together.
        try:
            # Is the passed in schedule type part of this program
            schedule_type = schedule_types.get(id=schedule_type_id)
        except ObjectDoesNotExist:
            # It's not, so let's just grab the first one 
            if  schedule_type_id is None or \
                request.user.user_type.id == UserType.USER or request.user.user_type.id == UserType.ADMIN:
                    if request.user.user_type.id == UserType.COACH:
                        date, master_day = CopyFromMaster.parseScheduleDate(schedule_date)
                        all_schedules = Schedule.objects.filter(date__in=[date, master_day], program__deleted=None)
                        assigned_to_schedule_types = ScheduledParticipant.objects.filter(schedule__in=all_schedules, scheduled_coach__user=request.user.id, \
                            schedule__schedule_type__deleted=None).values('scheduled_coach__schedule__schedule_type').distinct()
                        if len(assigned_to_schedule_types) > 0:
                            schedule_types = ScheduleType.objects.filter(id__in=assigned_to_schedule_types, program__deleted=None).order_by('start_time').select_related('program')
                            schedule_type = schedule_types.first()
                        else:
                            schedule_type = schedule_types.first()
                    else:
                        schedule_type = schedule_types.first()
            else:
                # For coaches (if schedule_type_id is passed), since we're displaying all of the schedule types, when there is a 
                # conflict, we're going to ignore the Program
                schedule_type = ScheduleType.objects.get(agency_id=self.request.user.agency_id, id=schedule_type_id)
        
        return schedule_date, program_id, schedule_type, schedule_types

    @transaction.atomic
    def post(self, request, format=None):
        scheduled_participants_expand = []
        serialized_unscheduled_participants = None
        scheduled_coaches = None
        scheduled_participants = None

        schedule_date, program, schedule_type, schedule_types = self._validateRequest(request)
        if request.user.user_type.id == UserType.USER or request.user.user_type.id == UserType.ADMIN:
            schedule_by_date = self.copying_from_master(schedule_date, program, schedule_type, request.user)
            scheduled_coaches = ScheduledCoach.objects.filter(schedule=schedule_by_date, user__program=program).select_related(
                'user',
                'location_start',
                'location_end',
                'skill',
                'schedule',
                'transportation_type').prefetch_related('scheduled_participants')
            scheduled_participants = ScheduledParticipant.objects.filter(schedule=schedule_by_date).select_related(
                'agency', 'user', 'user__transportation_type', 'scheduled_coach', 'schedule'
            ).prefetch_related('user__addresses')

            unscheduled_participants = self.get_missing_participants(schedule_by_date, program, scheduled_participants)
            serialized_unscheduled_participants = ScheduledParticipantSerializer(
                unscheduled_participants,
                many=True,
                expand=scheduled_participants_expand, )

        elif request.user.user_type.id == UserType.COACH or request.user.user_type.id == UserType.TRANSPORTER:

            # TODO: This only copies the schedule type requested. Should we loop and copy the remaining ones??
            schedule_by_date = self.copying_from_master(schedule_date, schedule_type.program.id, schedule_type, request.user)
            date, master_day = CopyFromMaster.parseScheduleDate(schedule_date)

            # Get the participants for the requested Schedule Type
            scheduled_coaches = ScheduledCoach.objects.filter(schedule=schedule_by_date,
                                                              user=request.user.id)

            scheduled_participants = ScheduledParticipant.objects.filter(scheduled_coach__in=scheduled_coaches)

            # To avoid cluttering the Coach UI, we should only return the schedule types where the Coach 
            # is actually assigned to.
            #
            # Since we don't copy ALL of the schedule types until someone requests it, we need to find
            # ALL of the schedules (master and daily) that have been created so far. Then we do a reverse
            # look up via the ScheduledPartcipants to see if the Coach is assigned any of them
            all_schedules = Schedule.objects.filter(date__in=[date, master_day], program__deleted=None)
            assigned_to_schedule_types = ScheduledParticipant.objects.filter(schedule__in=all_schedules, scheduled_coach__user=request.user.id, \
                                schedule__schedule_type__deleted=None).values('scheduled_coach__schedule__schedule_type').distinct()
            schedule_types = ScheduleType.objects.filter(id__in=assigned_to_schedule_types, program__deleted=None).order_by('start_time').select_related('program')

            scheduled_participants_expand = ['schedule_event_action_type']

        serialized_scheduled_coaches = ScheduledCoachSerializer(
            scheduled_coaches.order_by(Lower('user__first_name'), Lower('user__last_name')),
            many=True,
            context={'request': request})

        serialized_scheduled_participants = ScheduledParticipantSerializer(
            scheduled_participants.order_by(Lower('user__first_name'), Lower('user__last_name')),
            many=True,
            expand=scheduled_participants_expand,)
        
        serialized_schedule = ScheduleByAgencyAndDateSerializer(schedule_by_date)
        scheduled_event_logs = EventLog.objects.filter(schedule=schedule_by_date)
        serialized_event_logs = EventLogSerializer(scheduled_event_logs, many=True)
        serialized_schedule_types = ScheduleTypeSerializer(schedule_types, many=True)

        json_response = serialized_schedule.data
        json_response['scheduled_coaches'] = serialized_scheduled_coaches.data
        json_response['event_logs'] = serialized_event_logs.data
        json_response['schedule_types'] = serialized_schedule_types.data

        if serialized_unscheduled_participants:
            json_response['scheduled_participants'] = serialized_scheduled_participants.data + serialized_unscheduled_participants.data
        else:
            json_response['scheduled_participants'] = serialized_scheduled_participants.data

        for scheduled_participant in json_response['scheduled_participants']:
            scheduled_participant['id'] = str(scheduled_participant['user']) + '_' + str(scheduled_participant['schedule'])

        return Response(json_response, status=status.HTTP_200_OK)


class ScheduleViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.ADMIN, UserType.USER], ['GET', 'HEAD', 'OPTIONS']),
    ]),)
    serializer_class = ScheduleSerializer
    queryset = Schedule.objects.all()
    
    def get_queryset(self, *args, **kwargs):
        return Schedule.objects.filter(agency_id=self.request.user.agency_id)


class EventLogViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.ADMIN, UserType.USER],
                       UserPermission.NO_PERMISSIONS),
    ]),)
    serializer_class = EventLogSerializer
    queryset = EventLog.objects.all()

    def get_queryset(self, *args, **kwargs):
        return EventLog.objects.filter(agency_id=self.request.user.agency_id)


class EventLogAPIView(APIView):
    permission_classes = [IsAuthenticated]

    @transaction.atomic
    def post(self, request, format=None):
        try:
            user, schedule = request.data.get('id').split('_')
            participant = ScheduledParticipant.objects.select_related('scheduled_coach').get(user_id=user, schedule_id=schedule, agency_id=self.request.user.agency_id)
        except:
            participant = ScheduledParticipant.objects.select_related('scheduled_coach').get(pk=request.data.get('id'))

        scheduled_participant = ScheduledParticipantSerializer(participant, expand=["schedule_event_action_type"]).data
        current_event_type_id = scheduled_participant["schedule_event_action_type"]["id"]
        current_schedule_type = Schedule.objects.get(id=scheduled_participant["schedule"]).schedule_type
        
        def location_map_new(event):
            #get sort number of event and check if its start or end of the event and get location.
            schedule_type_event_sort_number = ScheduleTypeEventTypeAction.objects.filter(event_type= event, schedule_type= current_schedule_type).values_list('sort_order', flat=True)
            
            if schedule_type_event_sort_number.first() == '0' :
                return 'location_start_id'
            else:
                return 'location_end_id'


        def add_event(event_type_id):
            event_type = EventType.objects.get(pk=event_type_id)
            location = getattr(participant.scheduled_coach, location_map_new(event_type))
            
            event_log = EventLog(created_by=request.user, agency_id=self.request.user.agency_id,
                                 event_on_user=participant.user,
                                 event_type_id=event_type_id, schedule=participant.schedule,
                                 location_id=location)
            event_log.save()


        if current_event_type_id == EventType.NONE:
            return Response(scheduled_participant)
        else:
            add_event(current_event_type_id)

        participant = ScheduledParticipant.objects.get(pk=participant.id)
        return Response(ScheduledParticipantSerializer(participant, expand=["schedule_event_action_type"]).data)


class ScheduledCoachViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.ADMIN, UserType.USER], ['GET', 'HEAD', 'OPTIONS', 'PUT', 'PATCH']),
    ]),)
    serializer_class = ScheduledCoachSerializer

    def get_queryset(self, *args, **kwargs):
        coaches = ScheduledCoach.objects.filter(agency_id=self.request.user.agency_id)
        msg="The transportation in your schedule has been updated"
        isMaster = self.request.data.get("is_master")

        for coach in coaches:
            if coach.id == self.request.data.get("id"):
                if coach.user.send_notification_email is True:
                    if isMaster is None:
                        send_email_to_coach(coach.user.email, msg)

        return ScheduledCoach.objects.filter(agency_id=self.request.user.agency_id)

class ScheduledParticipantViewSet(FlexFieldsModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.ADMIN, UserType.USER], ['GET', 'HEAD', 'OPTIONS', 'PUT', 'PATCH']),
    ]),)
    serializer_class = ScheduledParticipantSerializer
    queryset = ScheduledParticipant.objects.all()

    def get_object(self):
        pk = self.request.parser_context['kwargs']['pk'] #pk =primary key
       
        if '_' not in pk:
            return super(ScheduledParticipantViewSet, self).get_object()

        scheduled_participant = None
        try:
            user, schedule = pk.split('_') # user and schedule are the IDs
            scheduled_participant = ScheduledParticipant.all_objects.get(user_id=user, schedule_id=schedule, agency_id=self.request.user.agency_id)
        except ScheduledParticipant.DoesNotExist:
            user, schedule = pk.split('_')
            scheduled_participant = ScheduledParticipant(
                                            schedule_id=schedule,
                                            user_id=user,
                                            agency_id=self.request.user.agency_id)
            scheduled_participant.save()
        
       
        return scheduled_participant

    def get_queryset(self, *args, **kwargs):
        return ScheduledParticipant.objects.filter(agency_id=self.request.user.agency_id)
    
    def update(self, request, *args, **kwargs):
        # django simple history for delay.
        isMaster = self.request.data.get("is_master")
        old_coach = self.get_object().scheduled_coach
        response = super().update(request, *args, **kwargs)
        new_coach = self.get_object().scheduled_coach
        coach_list = []

        if old_coach is not None:
            coach_list.append(old_coach)

        if new_coach is not None:
            coach_list.append(new_coach)

        msg="The participants in your schedule has been updated."

        for coach in set(coach_list):
            if coach.user.send_notification_email is True:
                if isMaster is None:
                    send_email_to_coach(coach.user.email, msg)
        
        return response

    # /api/v1/schedules/scheduled_participants/<pk>/bulk_update_status/
    @action(methods=['PATCH'], detail=True)
    def bulk_update_status(self, request, *args, **kwargs):
        obj = self.get_object()
        today = self.request.data.get("date")
        queryset = self.get_queryset().filter(user_id=obj.user_id, schedule__date=today).select_related("scheduled_coach")
        valid_statuses = [value for (value, label) in ScheduledParticipant.STATUSTYPES]
        updated_status = request.data.get('status')
        isMaster = None

        if self.request.data.get("is_master"):
            isMaster = self.request.data.get("is_master")

        coach_list = ScheduledCoach.objects.filter(scheduled_participants__in=queryset)
        coach_list = [*coach_list]
        
        if updated_status not in valid_statuses:
            return JsonResponse({'error': 'This status is not valid'}, status=status.HTTP_400_BAD_REQUEST)
        queryset.update(status=updated_status, scheduled_coach_id=None)

        coach_email_list = []
        msg="The participants in your schedule has been updated."

        for coach in coach_list:
            if coach.user.send_notification_email is True:
                coach_email_list.append(coach.user.email)

        if isMaster is None:
            for email in set(coach_email_list):
                send_email_to_coach(email, msg)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

class ScheduleTypeEventTypeActionViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER],
                       ['GET', 'HEAD', 'OPTIONS']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),             
    ]),)
    serializer_class = ScheduleTypeEventTypeActionSerializer
    # queryset = ScheduleTypeEventTypeAction.objects.all()
    def get_queryset(self, *args, **kwargs):
        return ScheduleTypeEventTypeAction.objects.filter(schedule_type__agency_id=self.request.user.agency_id)


@api_view(['GET'])
@permission_classes((partial(UserPermissionFactory, [
    UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER],['GET', 'HEAD', 'OPTIONS']),
    UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),             
    ]),))
def coach_calendar(request, coach_id, date_from, date_to):
    coach_id_int = int(coach_id)
    date_to_obj=datetime.strptime(date_to, "%Y-%m-%d").strftime('%Y-%m-%dT00:00:00.000Z')

    if date_from == date_to:
        master_range=[date_from, date_to]
    else: master_range=['1900-01-01', '1900-01-07']

    schedule_queryset = (Schedule.all_objects.filter(
            Q(date__range=[date_from, date_to]) | Q(date__range=master_range, modified_date__lte=date_to_obj),
            scheduledcoach__user_id=coach_id_int,
            scheduledparticipant__isnull=False)
        .select_related('program', 'schedule_type')
        .prefetch_related(
            Prefetch(
                'scheduledcoach_set',
                queryset=(ScheduledCoach.all_objects
                    .filter(user_id=coach_id_int)
                    .select_related('location_start', 'location_end', 'skill', 'user', 'transportation_type')),
                to_attr='coaches'
            ),

            Prefetch(
                'scheduledparticipant_set',
                queryset=(ScheduledParticipant.all_objects
                    .filter(scheduled_coach__user_id=coach_id_int)
                    .select_related('skill', 'user').annotate(
                        address=F('user__addresses__address'), city=F('user__addresses__city'), 
                        state=F('user__addresses__state'), zip=F('user__addresses__zip'), participant_note=F('note')
                    )),
                to_attr='participants'
            )
        ).distinct())

    def date_for_weekday(date):
        if date.year == 1900: 
            s_date = datetime.strptime(date_from, "%Y-%m-%d").date()
            e_date = datetime.strptime(date_to, "%Y-%m-%d").date()

            delta = e_date - s_date
            weekday = date.isoweekday()

            for i in range(delta.days + 1):
                day = s_date + timedelta(days=i)
                if day.isoweekday() == weekday:
                    return day
        else:
            return date

    results = []
    for schedule in schedule_queryset:
        if schedule.participants:
            schedule_date = date_for_weekday(schedule.date)
            data = {
            'participants': [],
            'skills': [],
            'addresses': [],
            'participant_status': [],
            'participant_notes': [],
            'title': schedule.schedule_type.type,
            'start': f'{schedule_date}T{schedule.schedule_type.start_time}',
            'end': f'{schedule_date}T{schedule.schedule_type.end_time}',
            'timeText': f'{schedule.schedule_type.start_time.strftime("%-I:%M %p")} - {schedule.schedule_type.end_time.strftime("%-I:%M %p")}',
            'program': schedule.program.name,
            'type_id': schedule.schedule_type.id
            }

            for coach in schedule.coaches:
                data.setdefault('start_location', getattr(coach.location_start, 'name', None))
                data.setdefault('end_location', getattr(coach.location_end, 'name', None))
                data.setdefault('transport_type', getattr(coach.transportation_type, 'name', None))
                if coach.note is not None:
                    data.setdefault('note', coach.note)

            for participant in schedule.participants:
                name = f'{participant.user.first_name} {participant.user.last_name}'
                data['participants'].append(name)
                data['participant_status'].append(f'{name}: {participant.status}')
                if participant.participant_note is not None: 
                    data['participant_notes'].append(f'{name}: {participant.participant_note}')
                if participant.skill is not None:
                    skill = participant.skill.__dict__['skill']
                    data['skills'].append(f'{name}: {skill}')
                if participant.address is not None:
                    data['addresses'].append(f'{name}: {participant.address}, {participant.city}, {participant.state} {participant.zip}')
                else:
                    data['addresses'].append(f'{name}: No address provided')

            data['participants'].sort()
            data['participant_status'].sort()
            data['participant_notes'].sort()
            data['skills'].sort()
            data['addresses'].sort()

            results.append(data) 

    distinct_results=[]

    for result in results:
        if result not in distinct_results:
            distinct_results.append(result)

    return JsonResponse(distinct_results, safe=False)