# Generated by Django 3.0.4 on 2020-08-12 03:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0039_auto_20200806_1226'),
    ]

    operations = [
        migrations.DeleteModel(
            name='HistoricalEventLog',
        ),
    ]
