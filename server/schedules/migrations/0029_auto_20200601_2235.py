# Generated by Django 3.0.4 on 2020-06-02 05:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0028_auto_20200601_1316'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalscheduledparticipant',
            name='schedule',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='schedules.Schedule'),
        ),
        migrations.AddField(
            model_name='scheduledparticipant',
            name='schedule',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='schedules.Schedule'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='scheduledparticipant',
            name='scheduled_coach',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='scheduled_participants', to='schedules.ScheduledCoach'),
        ),
    ]
