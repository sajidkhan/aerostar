# Generated by Django 3.0.4 on 2020-07-28 22:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('aerostar', '0025_auto_20200729_1503'),
        ('schedules', '0033_auto_20200729_1505'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventlog',
            name='location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='aerostar.Location'),
        ),
    ]
