# Generated by Django 3.0.4 on 2020-05-08 08:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('aerostar', '0012_auto_20200506_1233'),
        ('schedules', '0020_auto_20200506_1415'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scheduledcoach',
            name='location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='aerostar.Location'),
        ),
    ]
