# Generated by Django 2.2.9 on 2020-04-21 16:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0010_auto_20200421_0915'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scheduledcoach',
            name='agency',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='aerostar.Agency'),
        ),
    ]
