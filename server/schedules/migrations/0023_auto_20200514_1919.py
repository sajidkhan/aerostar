# Generated by Django 3.0.4 on 2020-05-15 02:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0022_auto_20200508_0211'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalscheduledcoach',
            name='note',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='historicalscheduledcoach',
            name='status',
            field=models.CharField(choices=[('scheduled', 'scheduled'), ('scheduled off', 'scheduled off'), ('unscheduled', 'unscheduled'), ('called out', 'called out')], default='scheduled', max_length=30),
        ),
        migrations.AddField(
            model_name='scheduledcoach',
            name='note',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='scheduledcoach',
            name='status',
            field=models.CharField(choices=[('scheduled', 'scheduled'), ('scheduled off', 'scheduled off'), ('unscheduled', 'unscheduled'), ('called out', 'called out')], default='scheduled', max_length=30),
        ),
    ]
