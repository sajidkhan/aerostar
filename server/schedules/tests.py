from django.utils import timezone
from schedules.models import EventType, EventLog, ScheduleType, Schedule, ScheduledCoach, ScheduledParticipant, \
    Location, Address, ScheduleTypeEventTypeAction
from utils.test_util import RestAPITestCase
from django.test import TestCase
from schedules.serializers import ScheduledParticipantSerializer
from aerostar.models import User, Agency, Program, UserType


class TestsSchedulesViewsTestCase(RestAPITestCase):

    def test_get_all_event_types(self):
        response = self.client.get('/api/v1/schedules/event_types/')
        self.assertMatchSnapshot(response)

    def test_get_event_type(self):
        event_type = EventType.objects.create(event='pickup')

        response = self.client.get(f'/api/v1/schedules/event_types/{str(event_type.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_event_type(self):
        response = self.client.post(f'/api/v1/schedules/event_types/', {'event': 'pickup'})
        self.assertMatchSnapshot(response)

    def test_put_event_type(self):
        event_type = EventType(event='pickup')
        event_type.save()

        response = self.client.put('/api/v1/schedules/event_types/999/', {'event': 'pickup'})
        self.assertMatchSnapshot(response)

        response = self.client.put(f'/api/v1/schedules/event_types/{event_type.id}/',
                                   data={'id': str(event_type.id), 'event': 'check_in'})
        self.assertMatchSnapshot(response)
        # Currently Not allowed to change event types
        # changed_event_type = EventType.objects.get(id=event_type.id)
        # self.assertEqual(changed_event_type.event, 'check_in')

    def test_delete_event_type(self):
        event_type = EventType(event='pickup')
        event_type.save()

        response = self.client.delete(f'/api/v1/schedules/event_types/999/')
        self.assertMatchSnapshot(response)

        response = self.client.delete(f'/api/v1/schedules/event_types/{str(event_type.id)}/')
        self.assertMatchSnapshot(response)

    def test_get_all_schedules_types(self):
        response = self.client.get('/api/v1/schedules/schedule_types/')
        self.assertMatchSnapshot(response)

    def test_get_schedules_type(self):
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        response = self.client.get(f'/api/v1/schedules/schedule_types/{str(schedule_type.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_schedules_type(self):
        response = self.client.post(f'/api/v1/schedules/schedule_types/',
                                    data={'type': 'start',
                                          'start_time': str(timezone.now()), 'end_time': timezone.now(),
                                          'agency': str(self.agency.id)})
        self.assertMatchSnapshot(response)

    def test_put_schedules_type(self):
        schedule_type = ScheduleType(type='start')
        schedule_type.agency = self.agency
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.save()

        response = self.client.put('/api/v1/schedules/schedule_types/999/', {'type': 'start'})
        self.assertMatchSnapshot(response)

        response = self.client.put(f'/api/v1/schedules/schedule_types/{schedule_type.id}/',
                                   data={'id': str(schedule_type.id), 'type': 'wrong'})
        self.assertMatchSnapshot(response)

        response = self.client.put(f'/api/v1/schedules/schedule_types/{schedule_type.id}/',
                                   data={'id': str(schedule_type.id), 'type': 'start',
                                         'start_time': str(timezone.now()), 'end_time': timezone.now(),
                                         'agency': str(self.agency.id)})
        self.assertMatchSnapshot(response)
        changed_schedule_type = ScheduleType.objects.get(id=schedule_type.id)
        self.assertEqual(changed_schedule_type.type, 'start')

    def test_delete_schedules_type(self):
        schedule_type = ScheduleType(type='start')
        schedule_type.agency = self.user.agency
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.save()

        response = self.client.delete(f'/api/v1/schedules/schedule_types/99999/')
        self.assertMatchSnapshot(response)

        response = self.client.delete(f'/api/v1/schedules/schedule_types/{str(schedule_type.id)}/')
        self.assertMatchSnapshot(response)

    def test_get_all_schedules(self):
        response = self.client.get('/api/v1/schedules/schedules/')
        self.assertMatchSnapshot(response)

    def test_get_schedules(self):
        # TODO: Fixtures!!
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        response = self.client.get(f'/api/v1/schedules/schedules/{str(schedule.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_schedule(self):
        # TODO: Fixtures!!
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        response = self.client.post(f'/api/v1/schedules/schedules/', {})
        self.assertMatchSnapshot(response)

        response = self.client.post(f'/api/v1/schedules/schedules/',
                                    data={'date': str(timezone.now().date()),
                                          'created_by': str(self.user.id),
                                          'agency': str(self.agency.id),
                                          'schedule_type': str(schedule_type.id)})
        self.assertMatchSnapshot(response)

    def test_put_schedule(self):
        # TODO: Fixtures!!
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        response = self.client.put('/api/v1/schedules/schedules/999/', {'type': 'start'})
        self.assertMatchSnapshot(response)

        response = self.client.put(f'/api/v1/schedules/schedules/{schedule.id}/',
                                   data={'id': str(schedule.id), 'created_by': str(self.user.id)})
        self.assertMatchSnapshot(response)

        response = self.client.put(f'/api/v1/schedules/schedules/{schedule.id}/',
                                   data={'id': str(schedule.id), 'date': str(timezone.now().date()),
                                         'created_by': str(self.user.id),
                                         'agency': str(self.agency.id),
                                         'schedule_type': str(schedule_type.id)})
        self.assertMatchSnapshot(response)

    def test_delete_schedule(self):
        # TODO: Fixtures!!
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        response = self.client.delete(f'/api/v1/schedules/schedules/99999/')
        self.assertMatchSnapshot(response)

        response = self.client.delete(f'/api/v1/schedules/schedules/{str(schedule.id)}/')
        self.assertMatchSnapshot(response)

    def test_get_all_event_logs(self):
        response = self.client.get('/api/v1/schedules/event_logs/')
        self.assertMatchSnapshot(response)

    def test_get_event_log(self):
        # TODO: Add Fixtures via factory_girl or plain old django fixtures!
        # Also might be worth just mocking the getter
        event_type = EventType.objects.create(event='pickup')

        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        event_log = EventLog()
        event_log.created_by = self.user
        event_log.event_on_user = self.user
        event_log.agency = self.user.agency
        event_log.event_type = event_type
        event_log.schedule = schedule
        event_log.location = location
        event_log.save()

        response = self.client.get(f'/api/v1/schedules/event_logs/{str(event_log.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_event_log(self):
        # TODO: Fixtures!!
        event_type = EventType.objects.create(event='pickup')
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        response = self.client.post(f'/api/v1/schedules/event_logs/', {})
        self.assertEqual(response.status_code, 403)

        response = self.client.post(f'/api/v1/schedules/event_logs/',
                                    data={'event_on_user': str(self.user.id),
                                          'created_by': str(self.user.id),
                                          'agency': str(self.agency.id),
                                          'event_type': str(event_type.id),
                                          'schedule': str(schedule.id),
                                          'location': str(location.id)})
        self.assertMatchSnapshot(response)

    def test_put_event_log(self):
        event_type = EventType.objects.create(event='pickup')

        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        event_log = EventLog()
        event_log.created_by = self.user
        event_log.event_on_user = self.user
        event_log.agency = self.user.agency
        event_log.event_type = event_type
        event_log.schedule = schedule
        event_log.location = location
        event_log.save()

        response = self.client.put('/api/v1/schedules/event_logs/999/', {'type': 'start'})
        self.assertMatchSnapshot(response)

        response = self.client.put(f'/api/v1/schedules/event_logs/{event_log.id}/',
                                   data={'id': str(event_log.id), 'created_by': str(self.user.id)})
        self.assertMatchSnapshot(response)

        response = self.client.put(f'/api/v1/schedules/event_logs/{event_log.id}/',
                                   data={'id': str(event_log.id), 'event_on_user': str(self.user.id),
                                         'created_by': str(self.user.id),
                                         'agency': str(self.agency.id),
                                         'event_type': str(event_type.id),
                                         'schedule': str(schedule.id),
                                         'location': str(location.id)})
        self.assertMatchSnapshot(response)

    def test_delete_event_log(self):
        # TODO: fixtures!!
        event_type = EventType.objects.create(event='pickup')

        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        event_log = EventLog()
        event_log.created_by = self.user
        event_log.event_on_user = self.user
        event_log.agency = self.user.agency
        event_log.event_type = event_type
        event_log.schedule = schedule
        event_log.location = location
        event_log.save()

        response = self.client.delete(f'/api/v1/schedules/event_logs/99999/')
        self.assertMatchSnapshot(response)

        response = self.client.delete(f'/api/v1/schedules/event_logs/{str(event_log.id)}/')
        self.assertMatchSnapshot(response)

    def test_get_all_scheduled_coach(self):
        response = self.client.get('/api/v1/schedules/scheduled_coach/')
        self.assertMatchSnapshot(response)

    def test_get_scheduled_coach(self):
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        scheduled_coach = ScheduledCoach()
        scheduled_coach.user = self.user
        scheduled_coach.agency = self.agency
        scheduled_coach.schedule = schedule
        scheduled_coach.location = location
        scheduled_coach.save()

        response = self.client.get(f'/api/v1/schedules/scheduled_coach/{str(scheduled_coach.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_scheduled_coach(self):
        # TODO: Fixtures!!
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        response = self.client.post(f'/api/v1/schedules/scheduled_coach/', {})
        self.assertMatchSnapshot(response)

        response = self.client.post(f'/api/v1/schedules/scheduled_coach/',
                                    data={'user_id': str(self.user.id),
                                          'agency': str(self.agency.id),
                                          'schedule': str(schedule.id),
                                          'location_id': str(location.id)})
        self.assertMatchSnapshot(response)

    def test_put_scheduled_coach(self):
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        scheduled_coach = ScheduledCoach()
        scheduled_coach.user = self.user
        scheduled_coach.agency = self.agency
        scheduled_coach.schedule = schedule
        scheduled_coach.location = location
        scheduled_coach.save()

        response = self.client.put('/api/v1/schedules/scheduled_coach/999/', {'type': 'start'})
        self.assertMatchSnapshot(response)

        response = self.client.put(f'/api/v1/schedules/scheduled_coach/{scheduled_coach.id}/',
                                   data={'id': str(scheduled_coach.id)})
        self.assertMatchSnapshot(response)

        response = self.client.put(f'/api/v1/schedules/scheduled_coach/{scheduled_coach.id}/',
                                   data={'id': str(scheduled_coach.id), 'user': str(self.user.id),
                                         'schedule': str(schedule.id),
                                         'location': str(location.id)})

        self.assertEqual(response.data['agency'], self.agency.id)
        self.assertMatchSnapshot(response)

    def test_delete_scheduled_coach(self):
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        scheduled_coach = ScheduledCoach()
        scheduled_coach.user = self.user
        scheduled_coach.agency = self.agency
        scheduled_coach.schedule = schedule
        scheduled_coach.location = location
        scheduled_coach.save()

        response = self.client.delete(f'/api/v1/schedules/scheduled_coach/99999/')
        self.assertMatchSnapshot(response)

        response = self.client.delete(f'/api/v1/schedules/scheduled_coach/{str(scheduled_coach.id)}/')
        self.assertMatchSnapshot(response)

    def test_get_all_scheduled_participants(self):
        response = self.client.get('/api/v1/schedules/scheduled_participants/')
        self.assertMatchSnapshot(response)

    def test_get_scheduled_participants(self):
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        event_type = EventType.objects.create(event='pickup',agency = self.user.agency)

        schedule_type_event_type_action = ScheduleTypeEventTypeAction()
        schedule_type_event_type_action.event_type = event_type
        schedule_type_event_type_action.schedule_type = schedule_type
        schedule_type_event_type_action.sort_order = 0
        schedule_type_event_type_action.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        scheduled_coach = ScheduledCoach()
        scheduled_coach.user = self.user
        scheduled_coach.agency = self.agency
        scheduled_coach.schedule = schedule
        scheduled_coach.location = location
        scheduled_coach.save()

        scheduled_participant = ScheduledParticipant()
        scheduled_participant.schedule = schedule
        scheduled_participant.user = self.user
        scheduled_participant.agency = self.agency
        scheduled_participant.scheduled_coach = scheduled_coach
        scheduled_participant.save()

        response = self.client.get(f'/api/v1/schedules/scheduled_participants/{str(scheduled_participant.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_scheduled_participants(self):
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        scheduled_coach = ScheduledCoach()
        scheduled_coach.user = self.user
        scheduled_coach.agency = self.agency
        scheduled_coach.schedule = schedule
        scheduled_coach.location = location
        scheduled_coach.save()

        response = self.client.post(f'/api/v1/schedules/scheduled_participants/',
                                    data={'user': str(self.user.id), 'agency': str(self.agency.id),
                                          'schedule': str(schedule.id),
                                          'scheduled_coach': str(scheduled_coach.id)})
        self.assertMatchSnapshot(response)

    def test_put_scheduled_participants(self, ignore_assert=False):
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()


        program = Program()
        program.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.program = program
        schedule.save()

        location = Location.objects.create(name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        scheduled_coach = ScheduledCoach()
        scheduled_coach.user = self.user
        scheduled_coach.agency = self.agency
        scheduled_coach.schedule = schedule
        scheduled_coach.location = location
        scheduled_coach.save()

        event_type = EventType.objects.create(event='pickup',agency = self.agency)

        schedule_type_event_type_action = ScheduleTypeEventTypeAction()
        schedule_type_event_type_action.event_type = event_type
        schedule_type_event_type_action.schedule_type = schedule_type
        schedule_type_event_type_action.sort_order = 0
        schedule_type_event_type_action.save()


        scheduled_participant = ScheduledParticipant()
        scheduled_participant.schedule = schedule
        scheduled_participant.user = self.user
        scheduled_participant.agency = self.agency
        scheduled_participant.scheduled_coach = scheduled_coach
        scheduled_participant.schedule_event_action_type = event_type
        scheduled_participant.save()

        response = self.client.put('/api/v1/schedules/scheduled_participants/999/', {'user': 999})
        if not ignore_assert:
            self.assertMatchSnapshot(response)

        event_type = EventType.objects.create(event='done',agency = self.agency)
        schedule_type_event_type_action = ScheduleTypeEventTypeAction()
        schedule_type_event_type_action.event_type = event_type
        schedule_type_event_type_action.schedule_type = schedule_type
        schedule_type_event_type_action.sort_order = 0
        schedule_type_event_type_action.save()

        response = self.client.put(f'/api/v1/schedules/scheduled_participants/{scheduled_participant.id}/',
                                   data={'id': str(scheduled_participant.id), 'user': str(self.user.id),
                                         'scheduled_coach': str(scheduled_coach.id),
                                         'schedule': str(schedule.id),
                                         'schedule_event_action_type': str(event_type.id)})
        if not ignore_assert:
            self.assertMatchSnapshot(response)
        return response

    def test_put_scheduled_participants_on_same_coach_but_different_programs(self):
        # putting a second scheduled participant with the same coach but different program should throw an error
        self.test_put_scheduled_participants(True)
        response = self.test_put_scheduled_participants(True)
        self.assertMatchSnapshot(response)

    def test_delete_scheduled_participants(self):
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        scheduled_coach = ScheduledCoach()
        scheduled_coach.user = self.user
        scheduled_coach.agency = self.agency
        scheduled_coach.schedule = schedule
        scheduled_coach.location = location
        scheduled_coach.save()

        scheduled_participant = ScheduledParticipant()
        scheduled_participant.schedule = schedule
        scheduled_participant.user = self.user
        scheduled_participant.agency = self.agency
        scheduled_participant.scheduled_coach = scheduled_coach
        scheduled_participant.save()

        response = self.client.delete(f'/api/v1/schedules/scheduled_participants/99999/')
        self.assertMatchSnapshot(response)

        response = self.client.delete(f'/api/v1/schedules/scheduled_participants/{str(scheduled_participant.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_schedule_by_date_when_schedule_is_present(self):
        # 1. User will need to query a date and get the schedule for that date.
        today = timezone.now().date()

        location = Location.objects.create(agency=self.agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        address = Address.objects.create(agency=self.agency, address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip', user=self.user)

        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.program = self.program
        schedule_type.save()

        schedule = Schedule()
        schedule.date = today
        schedule.created_by = self.user
        schedule.agency = self.user.agency
        schedule.schedule_type = schedule_type
        schedule.program = self.program
        schedule.save()

        scheduled_coach = ScheduledCoach()
        scheduled_coach.user = self.user
        scheduled_coach.agency = self.agency
        scheduled_coach.schedule = schedule
        scheduled_coach.location = location
        scheduled_coach.save()

        scheduled_participant = ScheduledParticipant()
        scheduled_participant.schedule = schedule
        scheduled_participant.user = self.user
        scheduled_participant.agency = self.agency
        scheduled_participant.scheduled_coach = scheduled_coach
        scheduled_participant.save()

        event_type = EventType.objects.create(event='pickup', agency= self.agency)
        schedule_type_event_type_action = ScheduleTypeEventTypeAction()
        schedule_type_event_type_action.event_type = event_type
        schedule_type_event_type_action.schedule_type = schedule_type
        schedule_type_event_type_action.sort_order = 0
        schedule_type_event_type_action.save()

        event_log = EventLog()
        event_log.created_by = self.user
        event_log.event_on_user = self.user
        event_log.agency = self.user.agency
        event_log.event_type = event_type
        event_log.schedule = schedule
        event_log.location = location
        event_log.save()

        data_params = {'date': str(today), 'schedule_type_id': schedule_type.id}
        response = self.client.post(f'/api/v1/schedules/schedule_by_date/', data=data_params, format='json')
        # TODO: Break up asserts!
        self.assertTrue(str(schedule.id) in str(response.content, encoding='utf8'))
        self.assertTrue(str(schedule_type.id) in str(response.content, encoding='utf8'))
        self.assertTrue(str(scheduled_coach.id) in str(response.content, encoding='utf8'))
        self.assertTrue(str(scheduled_participant.id) in str(response.content, encoding='utf8'))
        self.assertTrue(str(event_log.id) in str(response.content, encoding='utf8'))
        self.assertTrue(str(address.id) in str(response.content, encoding='utf8'))
        self.assertMatchSnapshot(response)
        # self.assertJSONEqual(
        #     str(response.content, encoding='utf8'),
        #     {"id": schedule.id,
        #      "deleted": schedule.deleted,
        #      "created_date": str(schedule.created_date),
        #      "modified_date": str(schedule.modified_date),
        #      "date": str(schedule.date),
        #      "is_master": False,
        #      "agency": str(schedule.agency.id),
        #      "created_by": str(schedule.created_by.id),
        #      "schedule_type": str(schedule.schedule_type.id)
        #      }
        # )

    def test_post_schedule_by_date_when_schedule_is_not_present(self):
        # 3. If a schedule date is fetched that does not exist then it should be created and returned.
        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = self.user.agency
        schedule_type.program = self.program
        schedule_type.save()

        Schedule.objects.all().delete()
        before_schedule_count = Schedule.objects.count()
        today = timezone.now().date()

        data_params = {'date': str(today), 'schedule_type_id': schedule_type.id}
        response = self.client.post(f'/api/v1/schedules/schedule_by_date/', data=data_params, format='json')
        after_schedule_count = Schedule.objects.count()
        # a master schedule and regular schedule is created
        self.assertTrue(before_schedule_count + 2 == after_schedule_count)
        self.assertMatchSnapshot(response)

    def test_post_schedule_by_date_when_schedule_is_present_as_coach(self):
        self.user.user_type = self.coach_type
        self.user.save()
        self.test_post_schedule_by_date_when_schedule_is_present()
        self.user.user_type = self.admin_type
        self.user.save()


#TODO: All possible states!
class TestsSchedulesSerializersTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_superuser('admin', 'admin@admin.com', 'admin123')
        agency = Agency.objects.create(name='foo')
        user.agency = agency
        user.save()

        schedule_type = ScheduleType(type='start')
        schedule_type.start_time = timezone.now()
        schedule_type.end_time = timezone.now()
        schedule_type.agency = user.agency
        schedule_type.save()

        schedule = Schedule()
        schedule.date = timezone.now().date()
        schedule.created_by = user
        schedule.agency = user.agency
        schedule.schedule_type = schedule_type
        schedule.save()

        location = Location.objects.create(agency=agency, name='foo', address='add1', address_two='address_two',
                                           city='city', state='state', zip='zip')

        scheduled_coach = ScheduledCoach()
        scheduled_coach.user = user
        scheduled_coach.agency = user.agency
        scheduled_coach.schedule = schedule
        scheduled_coach.location = location
        scheduled_coach.save()

        event_type = EventType.objects.create(event='pickup')

        scheduled_participant = ScheduledParticipant()
        scheduled_participant.user = user
        scheduled_participant.agency = user.agency
        scheduled_participant.scheduled_coach = scheduled_coach
        scheduled_participant.schedule_event_action_type = event_type
        scheduled_participant.save()
        self.scheduled_participant = scheduled_participant


    #TODO: All possible states!
    # def test_scheduled_participant_serializer_when_valid(self):
    #     subject = ScheduledParticipantSerializer(data=vars(self.scheduled_participant))
    #     self.assertTrue(subject.is_valid())

    # def test_scheduled_participant_serializer_when_invalid_dropoff_start(self):
    #     self.scheduled_participant.requires_mo_drop_off = True
    #     self.scheduled_participant.save()
    #
    #     schedule_type_event = ScheduleType(type='event')
    #     schedule_type_event.start_time = timezone.now()
    #     schedule_type_event.end_time = timezone.now()
    #     schedule_type_event.agency = self.scheduled_participant.user.agency
    #     schedule_type_event.save()
    #
    #     subject = ScheduledParticipantSerializer(data=self.scheduled_participant)
    #     self.assertTrue(subject.is_valid())

