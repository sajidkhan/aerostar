# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import GenericRepr, Snapshot


snapshots = Snapshot()

snapshots['TestsSchedulesViewsTestCase::test_delete_event_type 1'] = GenericRepr('<Response status_code=404, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_event_type 2'] = GenericRepr('<Response status_code=204>')

snapshots['TestsSchedulesViewsTestCase::test_get_all_event_logs 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_all_event_types 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_all_scheduled_coach 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_all_scheduled_participants 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_all_schedules 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_all_schedules_types 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_event_type 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_event_type 1'] = GenericRepr('<Response status_code=201, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_event_type 1'] = GenericRepr('<Response status_code=404, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_event_type 2'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_schedules_type 1'] = GenericRepr('<Response status_code=400, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_event_log 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_event_log 2'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_schedule 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_schedule 2'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_scheduled_coach 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_scheduled_coach 2'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_scheduled_participants 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_scheduled_participants 2'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_schedules_type 1'] = GenericRepr('<Response status_code=404, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_schedules_type 2'] = GenericRepr('<Response status_code=204>')

snapshots['TestsSchedulesViewsTestCase::test_get_event_log 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_scheduled_coach 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_scheduled_participants 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_schedules 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_schedules_type 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_event_log 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_schedule 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_schedule 2'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_scheduled_coach 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_scheduled_coach 2'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_scheduled_participants 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_event_log 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_event_log 2'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_event_log 3'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_schedule 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_schedule 2'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_schedule 3'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_scheduled_coach 1'] = GenericRepr('<Response status_code=404, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_scheduled_coach 2'] = GenericRepr('<Response status_code=400, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_scheduled_coach 3'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_scheduled_participants 1'] = GenericRepr('<Response status_code=404, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_scheduled_participants 2'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_scheduled_participants_on_same_coach_but_different_programs 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_schedules_type 1'] = GenericRepr('<Response status_code=404, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_schedules_type 2'] = GenericRepr('<Response status_code=400, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_schedules_type 3'] = GenericRepr('<Response status_code=400, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_schedule_by_date_when_schedule_is_not_present 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_schedule_by_date_when_schedule_is_present 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_schedule_by_date_when_schedule_is_present_as_coach 1'] = GenericRepr('<Response status_code=200, "application/json">')
