import calendar
from datetime import datetime
from schedules.models import Schedule, ScheduledCoach, ScheduledParticipant, EventLog

DATE_MAP = {
    'Monday': '1900-01-01',
    'Tuesday': '1900-01-02',
    'Wednesday': '1900-01-03',
    'Thursday': '1900-01-04',
    'Friday': '1900-01-05',
    'Saturday': '1900-01-06',
    'Sunday': '1900-01-07',
}


class CopyFromMaster:

    def __init__(self, request, program, schedule_by_date, schedule_date, schedule_type, all_participants):
        self.request = request
        self.schedule_by_date = schedule_by_date
        self.schedule_date = schedule_date
        self.schedule_type = schedule_type
        self.all_participants = all_participants
        self.program = program
        self.master_schedule_by_date = None
        self.copy_from_master()

    @staticmethod
    def parseScheduleDate(schedule_date):
        date = datetime.strptime(schedule_date, '%Y-%m-%d')
        day = date.weekday()
        return date, DATE_MAP[calendar.day_name[day]]

    def copy_from_master(self):
        if self.schedule_by_date.is_modified:
            return

        date, master_day = CopyFromMaster.parseScheduleDate(self.schedule_date)
        self.master_schedule_by_date, created = Schedule.objects.get_or_create(
            date=master_day,
            program_id=self.program,
            agency_id=self.request.user.agency_id,
            schedule_type=self.schedule_type, defaults={'created_by': self.request.user})

        self.copy_coaches_from_master()
        self.copy_participants_from_master()

        today_date = datetime.today()
        today_date = datetime.strftime(today_date, '%Y-%m-%d')
        check_date = datetime.strftime(date, '%Y-%m-%d')
        # Compare the actual dates YYYY-MM-DD without comparing the times to check if it is current day.
        if today_date == check_date:
            # Modifies the schedule so that it stops copying after the initial copy for TODAY
            self.schedule_by_date.is_modified = True
            self.schedule_by_date.save()

    def copy_coaches_from_master(self):
        # Return all coaches.  Add them to the scheduled table if they are missing
        current_coaches = ScheduledCoach.objects.filter(schedule=self.schedule_by_date).select_related('user')
        master_coaches = ScheduledCoach.objects.filter(schedule=self.master_schedule_by_date).select_related('user')

        coach_map = {}
        for coach in master_coaches:
            coach_map[coach.user.id] = coach

        for coach in current_coaches:
            master_coach = coach_map.get(coach.user.id)
            if master_coach:
                previous_status = master_coach.status
                coach.location_start = master_coach.location_start
                coach.location_end = master_coach.location_end
                coach.note = master_coach.note
                coach.skill = master_coach.skill
                coach.status = master_coach.status
                coach.transportation_type = master_coach.transportation_type
                coach.save()
                self.add_master_status_event(coach, previous_status)

    def copy_participants_from_master(self):
        master_participants = ScheduledParticipant.objects.filter(schedule=self.master_schedule_by_date).select_related(
            'user')
        scheduled_coaches = ScheduledCoach.objects.filter(schedule=self.schedule_by_date)

        participant_map = {}
        for participant in master_participants:
            participant_map[participant.user.id] = participant

        # Update existing participants
        for participant in self.all_participants:
            master_participant = participant_map.get(participant.user.id)
            if master_participant:
                try:
                    master_coach = master_participant.scheduled_coach.user.id
                except AttributeError:
                    master_coach = None
                try:
                    participant_coach = participant.scheduled_coach.user.id
                except AttributeError:
                    participant_coach = None

                if participant.note == master_participant.note and \
                    participant.status == master_participant.status and \
                        participant.requires_mo_drop_off == master_participant.requires_mo_drop_off and \
                        participant_coach == master_coach:
                    continue

                coach = None
                if master_participant.scheduled_coach:
                    for scheduled_coach in scheduled_coaches:
                        if scheduled_coach.user.id == master_participant.scheduled_coach.user.id:
                            coach = scheduled_coach

                previous_status = participant.status
                participant.scheduled_coach = coach
                participant.note = master_participant.note
                participant.status = master_participant.status
                participant.requires_mo_drop_off = master_participant.requires_mo_drop_off
                participant.skill = master_participant.skill
                participant.save()
                self.add_master_status_event(participant, previous_status)


    def add_master_status_event(self, scheduled_user, previous_status):
        if scheduled_user.status and scheduled_user.status != previous_status:
            event_log = EventLog(created_by=self.request.user, agency_id=scheduled_user.user.agency_id,
                                 event_on_user=scheduled_user.user,
                                 event_type_id=EventLog.STATUS_MAP[scheduled_user.status], schedule=scheduled_user.schedule)
            event_log.save()
