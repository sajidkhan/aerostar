from django.core.exceptions import ObjectDoesNotExist
from django.db.models.functions import Lower
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField
from rest_framework.relations import PrimaryKeyRelatedField

from aerostar.mixins import AddAgencyMixin, ScheduledUserUpdateMixin
from schedules.models import EventType, ScheduleType, Schedule, EventLog, ScheduledCoach, ScheduledParticipant, ScheduleTypeEventTypeAction
from aerostar.serializers import AddressSerializer, UserSerializer, SkillSerializer, LocationSerializer, \
    ScheduledCoachUserSerializer
from aerostar.models import Skill, Location, User
from rest_flex_fields import FlexFieldsModelSerializer
from transportation.serializers import TransportationTypeSerializer


class EventTypeSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = EventType
        fields = '__all__'


class ScheduleTypeSerializer(AddAgencyMixin, serializers.ModelSerializer):
    event_types_assigned = SerializerMethodField()

    def get_event_types_assigned(self, instance):
        items = ScheduleTypeEventTypeAction.objects.filter(schedule_type = instance )
        serializer = ScheduleTypeEventTypeActionSerializer(instance=items, many=True)
        return serializer.data

    class Meta:
        model = ScheduleType
        fields = '__all__'


class ScheduleSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = '__all__'


class ScheduleByAgencyAndDateSerializer(AddAgencyMixin, serializers.ModelSerializer):
    schedule_type = ScheduleTypeSerializer()

    class Meta:
        model = Schedule
        fields = '__all__'


class EventLogSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = EventLog
        fields = '__all__'


class ScheduledParticipantSerializer(ScheduledUserUpdateMixin, AddAgencyMixin, FlexFieldsModelSerializer):
    first_name = serializers.CharField(source='user.first_name', read_only=True)
    last_name = serializers.CharField(source='user.last_name', read_only=True)
    addresses = AddressSerializer(source='user.addresses', read_only=True, many=True, expand=['google_map_url'])
    transportation_type = TransportationTypeSerializer(source='user.transportation_type', read_only=True)
    schedule_modified_date = serializers.DateTimeField(source='schedule.modified_date', read_only=True)
    current_event_type = SerializerMethodField()
    schedule_event_action_type = SerializerMethodField()
    skill = SkillSerializer(read_only=True)
    skill_id = serializers.PrimaryKeyRelatedField(write_only=True, source='skill', queryset=Skill.objects.all(), required=False, allow_null=True)

    #new method to get custom assigned event types
    def get_schedule_event_action_type(self,obj):
        schedule = Schedule.objects.get(id=obj.schedule_id)
        #get event types list assigned to schedule type and order them by sort order.
        schedule_event_types = ScheduleTypeEventTypeAction.objects.filter(schedule_type= schedule.schedule_type).values_list('event_type', flat=True).order_by('sort_order')       
        try:
            event_logs = EventLog.objects.filter(schedule=schedule, event_on_user_id=obj.user_id, event_type__in=schedule_event_types).order_by('-created_date')
            current_event_log = event_logs.latest('id')
            current_event_type = current_event_log.event_type
        except ObjectDoesNotExist:
            current_event_type = None

        try:
            location_end = obj.scheduled_coach.location_end.id
        except AttributeError:
            location_end = None

        def event_map(current_event):
            if current_event == None:   
                #select first event type in order and return object                
                event_type = EventType.objects.get(id=schedule_event_types[:1])                
                return {'id': event_type.id, 'text': event_type.event, 'complete': False}
            else:
                #if theres only one event assigned to schedule type, set completed to true 
                if schedule_event_types.count() == 1:
                    event_type = EventType.objects.get(id=current_event.id)                    
                    return {'id': event_type.id, 'text': event_type.event, 'complete': True}
                else:
                    # get the current event order number from return next event based on order
                    schedule_events_pri = ScheduleTypeEventTypeAction.objects.filter(schedule_type= schedule.schedule_type)
                    current_schedule_event =schedule_events_pri.filter(event_type = current_event)
                    # If current event doesn't exist in scheduled type event(deleted by admin user), Check the Event Log to find the previous action 
                    # Verify if that previous action is still part of the Schedules Type, If that doesn't exist, keep doing that until you find the last action logged that still exist on the Schedule Type
                    
                    if current_schedule_event.count() == 0:
                        #remove current missing event from event logs
                        updated_event_logs = event_logs.exclude(event_type = current_event)
                        for event_log in updated_event_logs:
                            current_schedule_event =schedule_events_pri.filter(event_type = event_log.event_type)
                            if(current_schedule_event.count() != 0):
                                break;
                    #If current schedule event still doesn't exist in give events(if all the previous assigned events got deleted by admin)
                    #we consider the current event is None and return the first available event from schedule event types  
                    if current_schedule_event.count() == 0:
                        event_type = EventType.objects.get(id=schedule_event_types[:1])                
                        return {'id': event_type.id, 'text': event_type.event, 'complete': False}

                    current_event_order = current_schedule_event.values_list('sort_order',flat=True).first()                    
                    #check if this is the last event on schedule type, if so update completed to true else false. 
                    last_event_type_order = schedule_events_pri.order_by('-sort_order').values_list('sort_order',flat=True).first()
                    if(current_event_order == last_event_type_order):
                        event_type = EventType.objects.get(id=current_event.id)                    
                        return {'id': event_type.id, 'text': event_type.event, 'complete': True}
                    else:
                        next_schedule_event = schedule_events_pri.filter(sort_order = int(current_event_order)+1).values_list('event_type', flat=True).first()
                        event_type = EventType.objects.get(id=next_schedule_event) 
                    return {'id': event_type.id, 'text': event_type.event, 'complete': False}
            return 'None'
        return event_map(current_event_type)


    


    # Return the next event state to display based off of events in the event log
    def get_current_event_type(self, obj):
        schedule = Schedule.objects.get(id=obj.schedule_id)
        try:
            event_log = EventLog.objects.filter(schedule=schedule, event_on_user_id=obj.user_id, event_type__in=[EventType.ARRIVE, EventType.PICKUP, EventType.CHECK_IN]).latest('id')
            event_type = event_log.event_type.event
        except ObjectDoesNotExist:
            event_type = None

        try:
            location_end = obj.scheduled_coach.location_end.id
        except AttributeError:
            location_end = None

        def event_none():
            return {'id': EventType.PICKUP, 'text': 'Pick Up', 'complete': False}

        def event_pickup():
            return {'id': EventType.ARRIVE, 'text': 'Arrive', 'complete': False}

        def event_arrive():
            if obj.requires_mo_drop_off and location_end == Location.MAIN_OFFICE:
                return {'id': EventType.CHECK_IN, 'text': 'Check In', 'complete': False}
            else:
                return {'id': EventType.NONE, 'text': 'Arrived', 'complete': True}

        def event_done():
            if obj.requires_mo_drop_off and location_end == Location.MAIN_OFFICE:
                return {'id': EventType.NONE, 'text': 'Checked In', 'complete': True}
            else:
                return {'id': EventType.NONE, 'text': 'Arrived', 'complete': True}

        event_map = {
            None: event_none,
            'pickup': event_pickup,
            'arrive': event_arrive,
            'check_in': event_done,
        }

        return event_map[event_type]()

    class Meta:
        model = ScheduledParticipant
        fields = '__all__'
        expandable_fields = {
            'current_event_type': serializers.SerializerMethodField,
            'schedule_event_action_type': serializers.SerializerMethodField
        }


class ScheduledCoachSerializer(ScheduledUserUpdateMixin, AddAgencyMixin, serializers.ModelSerializer):
    scheduled_participants = SerializerMethodField()
    user = ScheduledCoachUserSerializer(read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(write_only=True, source='user', queryset=User.objects.all(),
                                                  required=False)
    skill = SkillSerializer(read_only=True)
    skill_id = serializers.PrimaryKeyRelatedField(write_only=True, source='skill', queryset=Skill.objects.all(),
                                                  required=False, allow_null=True)
    location_start = LocationSerializer(read_only=True)
    location_start_id = serializers.PrimaryKeyRelatedField(write_only=True, source='location_start',
                                                     queryset=Location.objects.all(), required=False, allow_null=True)
    location_end = LocationSerializer(read_only=True)
    location_end_id = serializers.PrimaryKeyRelatedField(write_only=True, source='location_end',
                                                     queryset=Location.objects.all(), required=False, allow_null=True)
    schedule_modified_date = serializers.DateTimeField(source='schedule.modified_date', read_only=True)

    def get_scheduled_participants(self, instance):
        scheduled_participants = instance.scheduled_participants.order_by(Lower('user__first_name'),
                                                                          Lower('user__last_name'))
        return map(lambda x: str(x.user_id) + '_' + str(x.schedule_id), scheduled_participants)

    class Meta:
        model = ScheduledCoach
        fields = '__all__'

class ScheduleTypeEventTypeActionSerializer(AddAgencyMixin, serializers.ModelSerializer):
    event_type_translation = SerializerMethodField()
    
    def get_event_type_translation(self, instance):
        event_type_trans = EventType.objects.filter(id = instance.event_type_id).latest('id')
        return event_type_trans.event

    class Meta:
        model = ScheduleTypeEventTypeAction
        fields = '__all__'