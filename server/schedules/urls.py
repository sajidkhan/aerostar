from django.urls import include, re_path, path
from rest_framework.routers import DefaultRouter
from django.conf.urls import url

from .views import EventTypeViewSet, ScheduleTypeViewSet, ScheduleViewSet, EventLogViewSet, ScheduledCoachViewSet, \
    ScheduledParticipantViewSet, ScheduleByDateAPIView, EventLogAPIView, ScheduleTypeEventTypeActionViewSet, coach_calendar

router = DefaultRouter()
router.register(r'event_types', EventTypeViewSet, basename='event_type')
router.register(r'event_logs', EventLogViewSet, basename='event_log')
router.register(r'schedule_types', ScheduleTypeViewSet, basename='schedule_type')
router.register(r'schedules', ScheduleViewSet, basename='schedule')
router.register(r'scheduled_coach', ScheduledCoachViewSet, basename='scheduled_coach')
router.register(r'scheduled_participants', ScheduledParticipantViewSet, basename='scheduled_participant')
router.register(r'schedule_type_event_type_actions', ScheduleTypeEventTypeActionViewSet, basename='schedule_type_event_type_action')

urlpatterns = [
    url(r'^schedule_by_date/$', ScheduleByDateAPIView.as_view(), name='schedule_by_date'),
    url(r'^next_state/$', EventLogAPIView.as_view(), name='next_state'),
    url(r'^coach_calendar/(?P<coach_id>\w+)/(?P<date_from>\d{4}-\d{2}-\d{2})/(?P<date_to>\d{4}-\d{2}-\d{2})/$', coach_calendar, name='coach_calendar'),
    re_path('', include(router.urls)),
]
