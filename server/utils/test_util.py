import datetime
from django.utils import timezone

from snapshottest.django import TestCase
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from aerostar.models import User, Agency, UserType, Program

"""
RestAPITestCase: Class is used for testing api endpoints. Assumptions:
 1. Test should be done on a registered superuser instance.
 2. API endpoint will use standard Django Token authentication.
 3. All Users will have an agency
"""
class RestAPITestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser('admin', 'admin@admin.com', 'admin123')
        self.coach_type = UserType(created_date=datetime.datetime.now(tz=timezone.utc))
        self.coach_type.id = 2
        self.coach_type.save()
        self.admin_type = UserType(created_date=datetime.datetime.now(tz=timezone.utc))
        self.admin_type.id = 4
        self.admin_type.save()
        self.user.user_type = self.admin_type
        self.token = Token.objects.create(user=self.user)
        self.client.force_login(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.agency = Agency.objects.create(name='foo')
        self.program = Program(name='Default', agency=self.agency);
        self.program.save()
        self.user.program.set([self.program])
        self.user.agency = self.agency
        self.user.save()

    def tearDown(self):
        User.objects.all().delete()
        Token.objects.all().delete()
