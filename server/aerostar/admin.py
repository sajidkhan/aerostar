from django.contrib import admin
from django.apps import apps
from server.settings import MY_APPS
from import_export.admin import ImportExportModelAdmin, ImportMixin, ImportForm, ConfirmImportForm
# from django.jQuery import jQuery
from aerostar.models import Location, Skill,Agency, User, UserType, Program, Address,Document, UserSkill,\
    Skill,LocationSkill
from aerostar.resources import LocationResource, SkillResource, ProgramResource, UserResource, AddressResource,\
    DocumentResource,UserSkillResource, SkillResource,LocationSkillResource
from django import forms


"""
    Auto Register all models
    see https://hackernoon.com/automatically-register-all-models-in-django-admin-django-tips-481382cf75e5    
    This code will automatically register all found models and apply the BaseAdmin ModelAdmin class to it.
    If a model is already registered, this code will skip it.

    Admin.py Standards
    1. All Models Should be registered in the admin UNLESS they are abstract only
    2. BaseAdmin contains the minimum requirements for every model
        a. All must be searchable by ID
        b. All columns should show in list_display
"""

admin.site.site_header = "Aerostar Administration"

class BaseAdmin(object):
    def __init__(self, model, admin_site):
        super(BaseAdmin, self).__init__(model, admin_site)

    class Meta:
        fields = '__all__'


# It's like reflection! For python! >:(
for my_app in MY_APPS:
    app = apps.get_app_config(my_app.split('.')[0])

    for my_model in app.models.items():
        admin_class = type('AdminClass', (BaseAdmin, admin.ModelAdmin), {})
        admin_class.list_display = [f.name for f in my_model[1]._meta.fields]
        admin_class.search_fields = ('id',)
        try:
            if not my_model[1]._meta.abstract and not my_model[1]._meta.auto_created:
                admin.site.register(my_model[1], admin_class)
        except admin.sites.AlreadyRegistered:
            pass


# Decorator function to re-register a model with a new ModelAdmin
# Should always be used in place of @admin.register()
# Example:
# from core.admin import re_register_model
# @re_register_model(SystemSettings)
# class TestAdmin(admin.ModelAdmin):
#     pass
def re_register_model(*models, site=None):
    def _model_admin_wrapper(new_admin_class):
        admin_site = site or admin.site
        admin_site.unregister(*models)
        admin_site.register(*models, admin_class=new_admin_class)

    return _model_admin_wrapper


@re_register_model(Location)
class LocationAdmin(ImportExportModelAdmin):
    list_display = ( 'name','full_address','agency','phone_number')
    search_fields = ("name","address","address_two","city","state","zip")
    list_filter = ("agency",)
    resource_class = LocationResource

@re_register_model(LocationSkill)
class LocationASkilldmin(ImportExportModelAdmin):
    list_display = ( 'skill','location','restriction_type')
    search_fields = ("location__name","skill__skill")
    list_filter = ("location__agency","skill","restriction_type")
    resource_class = LocationSkillResource

@re_register_model(Program)
class ProgramAdmin(ImportExportModelAdmin):
    list_display = ( 'name','agency')
    search_fields = ("name",)
    list_filter = ("agency",)
    resource_class = ProgramResource


# from django import forms

# TODO: All selected widgets do not work via: https://github.com/django-import-export/django-import-export/issues/1123
# For now, we will have to do this sort of thing on the csv side. Once this is fixed, we should import this back in!
# class CustomImportForm(ImportForm):
#     user_type = forms.ModelChoiceField(
#         queryset=UserType.objects.all(),
#         required=True)
#     program = forms.ModelChoiceField(
#         queryset=Program.objects.all(),
#         required=True)
#
#
# class CustomConfirmImportForm(ConfirmImportForm):
#     user_type = forms.ModelChoiceField(
#         queryset=UserType.objects.all(),
#         required=True)
#     program = forms.ModelChoiceField(
#         queryset=Program.objects.all(),
#         required=True)


@re_register_model(User)
class UserAdmin(ImportExportModelAdmin):
    list_display = ( 'id','full_name','deleted', 'username','email','agency','user_type','zone','transportation_type','is_active')
    list_filter = ("agency", "user_type","is_active")
    search_fields = ("first_name", "last_name", "username", "email","id",)
    resource_class = UserResource

# methos to filter  users based on agency selected on Initialization
class AddressAdminForm(forms.ModelForm):    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs.get("instance")
        if instance:
            if instance.agency:
                current_agency = Agency.objects.filter(pk=instance.agency.pk) 
            if current_agency:
                self.fields['user'].queryset = User.objects.filter(agency__in=current_agency)

                

@re_register_model(Address)
class AddressAdmin(ImportExportModelAdmin):
    form = AddressAdminForm
   
    list_display = ( 'full_address', 'user','agency')
    list_filter = ("agency", )
    search_fields = ("user__first_name", "user__last_name","user__username", "address","address_two","city","zip","state" )
    class Media:
        js = ('js/custom_scripts.js',) 
            
    resource_class = AddressResource

@re_register_model(Document)
class DocumentAdmin(ImportExportModelAdmin):
   
    list_display = ( 'type','expiration_date' ,'s_three_location', 'user','agency')
    list_filter = ("agency","type", )
    search_fields = ("user__first_name", "user__last_name","user__username", "type", )

    resource_class = DocumentResource
        
@re_register_model(UserSkill)
class UserskillAdmin(ImportExportModelAdmin):
    list_display = ( 'created_date','user','skill')
    search_fields = ("user__first_name","user__last_name","skill__skill" ,)
    list_filter = ("user__agency", "user__user_type","skill", )

    resource_class = UserSkillResource

@re_register_model(Skill)
class SkillAdmin(ImportExportModelAdmin):
    list_display = ( 'skill','agency')
    search_fields = ("skill", )
    list_filter = ("agency", )

    resource_class = SkillResource
