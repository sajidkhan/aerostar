# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import GenericRepr, Snapshot


snapshots = Snapshot()

snapshots['TestsSchedulesViewsTestCase::test_get_all_settings 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_setting 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_setting 1'] = GenericRepr('<Response status_code=201, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_setting_when_not_found 1'] = GenericRepr('<Response status_code=404, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_setting_when_request_invalid 1'] = GenericRepr('<Response status_code=400, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_setting_when_request_is_successful 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_setting 1'] = GenericRepr('<Response status_code=204>')

snapshots['TestsSchedulesViewsTestCase::test_get_all_user_types 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_user_type 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_user_type 1'] = GenericRepr('<Response status_code=201, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_user_type 1'] = GenericRepr('<Response status_code=204>')

snapshots['TestsSchedulesViewsTestCase::test_put_user_type 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_delete_address 1'] = GenericRepr('<Response status_code=204>')

snapshots['TestsSchedulesViewsTestCase::test_get_address 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_all_addresses 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_post_address 1'] = GenericRepr('<Response status_code=201, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_put_address 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_user_with_type 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['TestsSchedulesViewsTestCase::test_get_current_user_with_type 1'] = GenericRepr('<Response status_code=200, "application/json">')
