from django.db import transaction

from schedules.models import EventType, EventLog


class ScheduledUserUpdateMixin:

    @transaction.atomic
    def update(self, instance, validated_data):
        # Whenever a coach or participant is updated we mark the schedule as modified
        # so that we don't copy from it anymore
        instance.schedule.is_modified = True
        instance.schedule.save()
        if 'status' in validated_data and instance.status != validated_data['status']:
            user = self.context['request'].user
            event_on_user = instance.user
            event_log = EventLog(created_by=user, agency_id=event_on_user.agency_id,
                                 event_on_user=event_on_user,
                                 event_type_id=EventLog.STATUS_MAP[validated_data['status']], schedule=instance.schedule)
            event_log.save()
        return super(ScheduledUserUpdateMixin, self).update(instance, validated_data)


class AddAgencyMixin:
    # Automatically add the users Agency to requests.
    # This helps prevent users from modifying anything outside their agency
    def to_internal_value(self, data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        if user:
            # If a superuser passes agency then we will allow it.  They may be adding a new user to an agency
            if not (user.is_superuser and 'agency' in data):
                data['agency'] = user.agency.id
        else:
            try:
                del data['agency']
            except KeyError:
                pass

        return super(AddAgencyMixin, self).to_internal_value(data)
