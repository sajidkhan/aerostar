from django.contrib.auth.tokens import default_token_generator
from templated_mail.mail import BaseEmailMessage
from djoser import utils
from djoser.email import PasswordResetEmail, PasswordChangedConfirmationEmail
from djoser.conf import settings


class AerostarCreateUserPasswordResetEmail(BaseEmailMessage):
    template_name = "create_user_password_reset.html"

    def get_context_data(self):
        context = super().get_context_data()
        user = context.get("user")
        context["uid"] = utils.encode_uid(user.pk)
        context["token"] = default_token_generator.make_token(user)
        context["url"] = settings.PASSWORD_RESET_CONFIRM_URL.format(**context)
        return context


class AerostarPasswordResetEmail(PasswordResetEmail):
    template_name = "password_reset.html"


class AerostarPasswordChangedConfirmationEmail(PasswordChangedConfirmationEmail):
    template_name = "password_changed_confirmation.html"
