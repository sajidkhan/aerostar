import datetime
from django.db.models import Q, Value
from django.db.models.functions import Concat
from django.db import connection
from .models import User, Program
from schedules.models import ScheduledCoach, ScheduledParticipant, Schedule

DATE_MAP = {
    '1900-01-01': 'Monday',
    '1900-01-02': 'Tuesday',
    '1900-01-03': 'Wednesday',
    '1900-01-04': 'Thursday',
    '1900-01-05': 'Friday', 
    '1900-01-06': 'Saturday',
    '1900-01-07': 'Sunday' 
}

def participant_activity_sql():
    return """
with service_by_day as (
    select se.event_on_user_id, max(se.created_date) - min(se.created_date) as hrs_diff, date_trunc('day', se.created_date at time zone 'pst' ) as service_date, ss.program_id
    from schedules_eventlog se 
    inner join schedules_schedule ss on ss.id  = se.schedule_id
    inner join schedules_scheduledcoach ssc on ssc.schedule_id  = ss.id and se.created_by_id = ssc.user_id 
    where se.created_date BETWEEN %s AND %s
          and se.agency_id = %s
    group by se.event_on_user_id, date_trunc('day', se.created_date  at time zone 'pst'), ss.program_id
)
    """

def participant_by_day(agency_id, date_from, date_to):
    date_from = date_from.strftime("%Y-%m-%d")
    date_to = (datetime.timedelta(days=1) + date_to).strftime("%Y-%m-%d")

    with connection.cursor() as cursor:
        sql = participant_activity_sql() + """

select au.first_name || ' ' || au.last_name as Participant, 
to_char(scd.service_date, 'YYYY/MM/DD') as ServiceDate, to_char(scd.hrs_diff, 'HH24:MI') as HoursServiced
from service_by_day scd
inner join aerostar_user au on au.id = scd.event_on_user_id 
inner join aerostar_program ap on ap.id = scd.program_id
group by Participant, ServiceDate, scd.hrs_diff
order by Participant;
        """

        cursor.execute(sql, [date_from, date_to, agency_id])
        data = cursor.fetchall()

    columns = ["Participant", "ServiceDate", "HoursServiced"]
    return (columns, data)


def participant_by_month(agency_id, date_from, date_to):
    date_from = date_from.strftime("%Y-%m-%d")
    date_to = (datetime.timedelta(days=1) + date_to).strftime("%Y-%m-%d")

    with connection.cursor() as cursor:
        sql = participant_activity_sql() + """

select au.first_name || ' ' || au.last_name as "Participant", ap."name" as "Program", to_char(sum(scd.hrs_diff), 'HH24:MI') as "Total Hours" 
from service_by_day scd
inner join aerostar_user au on au.id = scd.event_on_user_id 
inner join aerostar_program ap on ap.id = scd.program_id 
group by "Participant", "Program"
order by "Participant"
        """

        cursor.execute(sql, [date_from, date_to, agency_id])
        data = cursor.fetchall()

    columns = ["Participant", "Program", "Total Hours"]
    return (columns, data)

def all_coaches():
    return """
        select first_name || ' ' || last_name as coach from aerostar_user
        where user_type_id = 2 or user_type_id = 5
        order by last_name asc;
    """

def coach_activity_sql():
    return """
        select sc.created_date, s.date, au.first_name || ' ' || au.last_name as coach, sc.user_id, sc.schedule_id, sc.status
        from schedules_scheduledcoach sc
        inner join schedules_schedule s on s.id = sc.schedule_id
        inner join aerostar_user au on au.id = sc.user_id
        where sc.agency_id = %s and sc.created_date between %s and %s or (sc.created_date < %s and s.date between '1900-01-01' and '1900-01-07')
    """

def coaches_by_day(agency_id, date_from, date_to):
    date_from_str = date_from.strftime("%Y-%m-%d")
    date_to_str = (datetime.timedelta(days=1) + date_to).strftime("%Y-%m-%d")

    month = date_from.month
    year = date_from.year

    with connection.cursor() as cursor:
        sql = coach_activity_sql()
        cursor.execute(sql, [agency_id, date_from_str, date_to_str, date_to_str])
        data = cursor.fetchall()

        sql = all_coaches()
        cursor.execute(sql)
        coaches = cursor.fetchall()

    days_in_month = {x: set() for x in range(date_from.day, date_to.day + 1)}

    # Coaches by day
    for d in data:
        if d[1].year != 1900 and d[1].month == date_from.month:
            # Appends coaches name and status given day
            days_in_month[d[1].day].add((d[2], d[5]))
        elif d[1].year == 1900:
            for day in days_in_month: 
                map_key = d[1].strftime("%Y-%m-%d")
                weekday = datetime.date(year, month, day).strftime('%A')

                if weekday == DATE_MAP[map_key]:
                    days_in_month[day].add((d[2], d[5]))

    # Add missing coaches by day
    days_in_month_new = {x: set() for x in range(date_from.day, date_to.day + 1)} 
    
    coach_list = [c[0] for c in coaches]

    for day in days_in_month:
        for coach in coach_list:
            if (coach, 'called_out') in days_in_month[day]:
                days_in_month_new[day].add((coach, 'called_out'))
            elif (coach, 'scheduled') in days_in_month[day]:
                days_in_month_new[day].add((coach, 'scheduled'))
            elif (coach, 'scheduled_off') in days_in_month[day] :
                days_in_month_new[day].add((coach, 'scheduled_off'))
            else:
                days_in_month_new[day].add((coach, 'unscheduled'))

    # Convert to rows
    data = []
    for day in days_in_month_new:
        for row in days_in_month_new[day]:
            data.append((row[0], row[1], day))

    columns = ['Coach', 'Status', 'ServiceDate']
    return (columns, data)

def all_schedules_by_date(agency_id, date, user_type):
    weekday = date.strftime("%A")
    str_date = date.strftime("%Y-%m-%d")
    weekday_to_date = dict(zip(DATE_MAP.values(), DATE_MAP.keys()))
    mapped_weekday = weekday_to_date[weekday]

    formatted_data = []
    columns = []

    if user_type == 'Participants':
        data = (ScheduledParticipant.objects
            .filter((Q(schedule__date=str_date) | Q(schedule__date=mapped_weekday)), agency=agency_id)
            .annotate(
                name=Concat('user__first_name', Value(' '), 'user__last_name'),
                coach=Concat(
                    'scheduled_coach__user__first_name',
                     Value(' '),
                    'scheduled_coach__user__last_name'
                )
            )
            .values_list(
                'name',
                'coach',
                'schedule__program__name',
                'schedule__schedule_type__type',
                'status'
            )
            .distinct()
        )

        columns = ['name', 'coach', 'program', 'scheduleType', 'status', 'key']
        for i in data:
            formatted_data.append((*i[0:5], i[0] + '_' + str(datetime.datetime.now())))
    else:
        data = (ScheduledCoach.objects
            .filter((Q(schedule__date=str_date) | Q(schedule__date=mapped_weekday)), agency=agency_id)
            .annotate(name=Concat('user__first_name', Value(' '), 'user__last_name'))
            .values_list(
                'name',
                'schedule__program__name',
                'schedule__schedule_type__type',
                'status'
            )
            .distinct()
        )

        columns = ['name', 'program', 'scheduleType', 'status', 'key']
        for i in data:
            formatted_data.append((*i[0:4], i[0] + '_' + str(datetime.datetime.now())))

    return (columns, formatted_data)