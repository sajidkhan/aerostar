# Generated by Django 2.2.9 on 2020-04-24 16:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('aerostar', '0010_auto_20200423_1608'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='agency',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='aerostar.Agency'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='historicaladdress',
            name='agency',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='aerostar.Agency'),
        ),
    ]
