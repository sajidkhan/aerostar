# Generated by Django 3.0.4 on 2021-01-27 19:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aerostar', '0031_auto_20201103_1540'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='program',
            field=models.ManyToManyField(blank=True, related_name='users', to='aerostar.Program'),
        ),
    ]
