from datetime import datetime, timedelta
import csv
from datetime import date
from django.http import JsonResponse, Http404
from django.db import transaction
from django.db.models import Q
from djangorestframework_camel_case.parser import CamelCaseJSONParser
from django import forms
from django.http import HttpResponse

from rest_framework import viewsets, permissions, views, response, status
from rest_framework.decorators import action
from server.multipart_formencode_parser import MultipartFormencodeParser
from .serializers import UserSerializer, CurrentUserSerializer, SettingSerializer, UserTypeSerializer, \
    AddressSerializer, LocationSerializer, \
    RestrictionTypeSerializer, LocationSkillSerializer, SkillSerializer, UserSkillSerializer, DocumentSerializer, \
    ProgramSerializer, AgencySerializer, UserSerializerAdmin, GroupSerializer, UserGroupSerializer
from .models import User, Setting, UserType, Address, Location, RestrictionType, LocationSkill, Skill, UserSkill, \
    Document, Program, Agency, Group, UserGroup
from schedules.copy_from_master import DATE_MAP as MASTER_SCHEDULE_DATES
from schedules.models import ScheduledCoach, ScheduledParticipant, Schedule

from rest_framework.response import Response
from aerostar.permissions import UserPermissionFactory, UserPermission
from functools import partial
from djoser.conf import settings
from djoser.compat import get_user_email
from django.contrib.auth import get_user_model, logout, authenticate, login
from rest_framework.permissions import IsAdminUser
from rest_framework.decorators import action
from itertools import chain

import aerostar.reports as reports

class UserViewSet(viewsets.ModelViewSet):
    parser_classes = [MultipartFormencodeParser, CamelCaseJSONParser]

    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER],
                       UserPermission.NO_PERMISSIONS),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
    ]),)
    serializer_class = UserSerializer
    queryset = User.objects.all()
    filterset_fields = ['user_type']

    def create(self, request, *args, **kwargs):
        request.data.update({'username': request.data['first_name'] + request.data['last_name']})
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        new_user = serializer.save()
        new_user.is_active = True

        # User MUST have a password in order to move forward with the password reset process.
        # However, we do not allow admins to set other users passwords.
        # We will generate the password on the fly and set it for the password reset process ONLY.
        static_user = get_user_model()
        password = static_user.objects.make_random_password()
        new_user.set_password(password)
        new_user.save()

        # if a coach/transporter, create the user and then send out password reset operation.
        if new_user.user_type.id in [UserType.COACH, UserType.TRANSPORTER, UserType.ADMIN, UserType.USER]:
            context = {"user": new_user}
            to = [get_user_email(new_user)]
            settings.EMAIL.activation(self.request, context).send(to)
            return Response(serializer.data, status=201)

        # if any other type of user (participant), then go through normal creation process
        # WITHOUT sending a password reset.
        return Response(serializer.data, status=201)

    def get_queryset(self, *args, **kwargs):
        return User.objects.filter(agency_id=self.request.user.agency_id)


class UserAdminViewSet(UserViewSet):
    permission_classes = (IsAdminUser,)
    serializer_class = UserSerializerAdmin


class CoachViewSet(UserViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.USER], ['GET', 'HEAD', 'OPTIONS']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
    ]),)

    def get_queryset(self, *args, **kwargs):
        return User.all_objects.filter(agency_id=self.request.user.agency_id,
                                   user_type__in=[UserType.TRANSPORTER, UserType.COACH])

    @transaction.atomic
    def destroy(self, request, pk=None):
        if pk is None:
            return Response(status=404)
        else:
            '''
            When a Coach is deleted, we need to release any ScheduledParticipants that have been scheduled
            IN THE FUTURE and ON MASTER SCHEDULES. If we don't, they will not show up anymore since the system
            will think they are still scheduled (which they technically are...to deleted coaches)
                See server/schedules/views.py - add_missing_participants

            We need to manually handle this. We cannot depend on the Django Model to do it,
            because of the Database CASCADE on the Models that we need to undo.

            NOTE: By delete, we mean `SOFT DELETE`. The `deleted` column gets populated and filtered out on queries by
                django-safedetele library.

            If we do:
                user.delete()
            this will trigger a soft delete on **ALL** ScheduledParticipants, followed by ScheduledCoaches. Because
            we should not modify any PREVIOUS assignments, this doesn't work for us.

            So we have to:
                * Grab all the ScheduledCoaches for the Master schedule and future dates
                * Soft delete them, which will trigger the cascade delete on ScheduledParticipants
                * Fetch the ScheduledParticipants that were just deleted and release them back into the Participant List
            
            NOTE: I think we can do also do
                    user.delete()
                   ScheduledCoach.all_objects   <---- Include deleted objects
                   // un-delete participants

                but I'm tired and I already tested the current workflow extensively, don't want to re-test
                and I just want this pushed 
            '''

            coachSchedules = ScheduledCoach.objects.select_related('schedule').filter(
                Q(schedule__date__in=MASTER_SCHEDULE_DATES.values()) | Q(schedule__date__gt=datetime.now()),
                agency_id=request.user.agency_id,
                user=pk,
            )
            ids = []
            for coachSchedule in coachSchedules:
                ids.append(coachSchedule.id)
                # safe delete the coach card
                # causes a cascading delete on the ScheduledPartipant Table!
                coachSchedule.delete()
            # Clear participants paired with "deleted" coaches, and un-soft-delete them
            # (https://django-safedelete.readthedocs.io/en/latest/models.html#safedelete.models.SafeDeleteModel.save)
            scheduledParticipant = ScheduledParticipant.all_objects.filter(scheduled_coach__in=ids)

            for participant in scheduledParticipant:
                participant.scheduled_coach = None
                participant.save()

            user = User.objects.filter(id=pk).first()
            # TODO: This does no prepopulate deleted column, something is overwritting it
            user.delete()
            # Let's use this property to determine if the user is "deleted"
            # user.is_active = False
            # user.save() 

        return Response(status=200)


class ParticipantViewSet(UserViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.USER, UserType.COACH], ['GET', 'HEAD', 'OPTIONS']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
    ]),)

    def get_queryset(self, *args, **kwargs):
        return User.all_objects.filter(agency_id=self.request.user.agency_id,
                                   user_type=UserType.PARTICIPANT)

    @action(detail=True)
    def history(self, request, pk):
        ## ENDPOINT:
        # /participant/{id}/history/
        history_record = User.history.filter(id=pk)[:10]
        history_address = Address.history.filter(user_id=pk)[:10]

        comparison_info = getParticipantHistory(history_record)
        comparison_address = getParticipantHistory(history_address)

        merged_comparison = comparison_info + comparison_address
        grouped_comparison = groupChangesByTimeframe(merged_comparison)

        return Response(grouped_comparison)
    
    @transaction.atomic
    def destroy(self, request, pk=None):
        if pk is None:
            return Response(status=404)
        else:

            '''
            * We should update users deleted
            * We also should unlink/release and update delete deleted participant from the future schedule dates. 
            * We should not update delete or change any of the past schedule dates of deleted participant. 
            * We should not update delete column of any of the event logs of participant.(updated cascade on eventlog model for users to PROTECT)
            '''
            user = User.objects.filter(id=pk).first()
            user.delete()
            ''' Schedule Participant unlink/release coach if any of the schedule date is in future/current'''

            FutureScheduledParticipant = ScheduledParticipant.objects.select_related('schedule').filter(
                Q(schedule__date__in=MASTER_SCHEDULE_DATES.values()) | Q(schedule__date__gt=datetime.now()),
                user = pk)
            
            for participant in FutureScheduledParticipant:
                participant.scheduled_coach = None                
                participant.save()
                participant.delete()          
            
            return Response(status=200)



def getParticipantHistory(history_record):
    '''
    Takes in a historical record array and extracts the modifications. 
    Logic has an outer while loop that uses an inner for loop to scan
    the historical records for modifications. Then, if a modification is found when scanning with the for loop, that
    change will be recorded and the for loop will break out updating the outer while loop to jump to a corresponding checkpoint
    to begin scanning again until the whole historical record array is scanned. NOTE: history array is currently
    hardcoded to only pull first 10 current records at most.
    '''
    # Initialize noMoreRecords to FALSE: if TRUE, inner for loop has scanned the whole historical array and has detected no record modifications and should break out of the while loop.
    noMoreRecords = False
    changes = []
    i = 0
    # Condition for length - 1 because we don't need to run last index. No more index to compare records.
    while i < len(history_record) - 1:
        # Initialize variables to record history modifications if any is found
        changed = []
        # The scanning for loop used to find historical differences
        for j in range(i, len(history_record)):
            new_record = history_record[i]
            old_record = history_record[j]
            delta = new_record.diff_against(old_record)
            if delta.changes:
                for change in delta.changes:
                    history_modification = "{} changed from {} to {}".format(change.field, change.old, change.new)
                    changed.append(history_modification)
                first_name = User.objects.filter(id=history_record[i].history_user_id)[0].first_name
                last_name = User.objects.filter(id=history_record[i].history_user_id)[0].last_name
                changes.append({
                'id': new_record.history_id,
                'createdBy': f"{first_name} {last_name}",
                'date': (history_record[i].modified_date - timedelta(hours=8)).strftime("%m-%d-%Y %H:%M"),
                'date_object': new_record.modified_date,
                'changed': changed
                })
                # Update while loop scanning checkpoint and break out of inner for loop
                i = j - 1
                break
            # Jump while loop to breakout condition if no changes were scanned from current checkpoint
            if j == len(history_record) - 1:
                noMoreRecords = True
        if noMoreRecords:
            i = len(history_record)
        else:
            i = i+1

    return changes


def groupChangesByTimeframe(records):
    '''
    Group historical record modification by same editor & date
    '''
    for i in range(0, len(records)):
        # If modified date matches within a minute boundary and also the same person making the modification, categorize changes as a series of changes (update existing output) and pop existing duplicate object.
        j = i + 1
        while j < len(records):
            if records[i]['date'] == records[j]['date'] and records[i]['createdBy'] == records[j]['createdBy']:
                for item in records[j]['changed']:
                    records[i]['changed'].append(item)
                records.pop(j)
            else:
                j = j + 1

    return records


class CurrentUserViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER], ['GET', 'HEAD', 'OPTIONS']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
    ]),)
    serializer_class = CurrentUserSerializer
    queryset = User.objects.all()

    def list(self, request, *args, **kwargs):
        serializer = CurrentUserSerializer(instance=request.user)
        return Response(serializer.data)


class SettingViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.ADMIN, UserType.USER],
                       UserPermission.NO_PERMISSIONS),
    ]),)
    serializer_class = SettingSerializer
    queryset = Setting.objects.all()

    def get_queryset(self, *args, **kwargs):
        return Setting.objects.filter(user=self.request.user)


class UserTypeViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UserTypeSerializer
    queryset = UserType.objects.all()


class ProgramViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER], ['GET', 'HEAD', 'OPTIONS']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
    ]),)
    serializer_class = ProgramSerializer
    queryset = Program.objects.all()

    def get_queryset(self, *args, **kwargs):
        target = self.request.GET.get('state', '')
        if target == 'schedule':
            return Program.objects.filter(agency_id=self.request.user.agency_id).order_by('name')
        else:
            return Program.all_objects.filter(agency_id=self.request.user.agency_id).order_by('-deleted', 'name')
            
    @action(detail=True, methods=['post'])
    def assign_users(self, request, pk):
        ## ENDPOINT:
        # /aerostar/program/{id}/assign_users/
        # BODY:
        # { 'users': [<participant/coach IDs>]}
        try:
            program = Program.objects.get(id=pk)
            program.users.clear()
            program.users.add(*request.data['users'])
        except:
            return Response(status=400)
        return Response(status=200)
        

class AddressViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER], UserPermission.NO_PERMISSIONS),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
    ]),)
    serializer_class = AddressSerializer
    queryset = Address.objects.all()


class LocationViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER], ['GET', 'HEAD', 'OPTIONS']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
    ]),)
    serializer_class = LocationSerializer
    queryset = Location.objects.all()

    def get_queryset(self, *args, **kwargs):
        return Location.objects.filter(Q(agency_id=self.request.user.agency_id) | Q(agency_id__isnull=True))


class RestrictionTypeViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.ADMIN, UserType.USER],
                       UserPermission.NO_PERMISSIONS),
    ]),)
    serializer_class = RestrictionTypeSerializer
    queryset = RestrictionType.objects.all()


class LocationSkillViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.ADMIN, UserType.COACH, UserType.TRANSPORTER, UserType.USER],
                       UserPermission.NO_PERMISSIONS),
    ]),)
    serializer_class = LocationSkillSerializer
    queryset = LocationSkill.objects.all()


class SkillViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER], UserPermission.NO_PERMISSIONS),
        UserPermission([UserType.ADMIN, UserType.USER], ['GET', 'HEAD', 'OPTIONS', 'POST', 'PATCH']),
    ]),)
    serializer_class = SkillSerializer
    queryset = Skill.objects.all()

    def get_queryset(self, *args, **kwargs):
        return Skill.objects.filter(agency_id=self.request.user.agency_id)


class UserSkillViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.ADMIN, UserType.COACH, UserType.TRANSPORTER, UserType.USER],
                       UserPermission.NO_PERMISSIONS),
    ]),)
    serializer_class = UserSkillSerializer
    queryset = UserSkill.objects.all()

class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER], UserPermission.NO_PERMISSIONS),
        UserPermission([UserType.ADMIN, UserType.USER], ['GET', 'HEAD', 'OPTIONS', 'POST', 'PATCH','DELETE']),
    ]),)
    serializer_class = GroupSerializer
    queryset = Group.objects.all()

    def get_queryset(self, *args, **kwargs):
        return super().get_queryset().filter(agency_id=self.request.user.agency_id)


class UserGroupViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER], ['GET', 'HEAD', 'OPTIONS']),
        UserPermission([UserType.ADMIN, UserType.USER], ['GET', 'HEAD', 'OPTIONS', 'POST', 'PATCH']),
    ]),)
    serializer_class = UserGroupSerializer
    queryset = UserGroup.objects.all()

    @action(detail=True, methods=['get'])
    def assigned_coaches(self, request, pk):
        assigned = super().get_queryset().filter(group_id=pk)
        serializer = self.serializer_class(assigned, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['post'])
    def assign_coaches(self, request, pk):
        try:
            super().get_queryset().filter(group_id=pk).delete()

            for coach in request.data['coaches']:
                UserGroup.objects.create(group_id=pk, user_id=coach)
        except:
            return Response(status=400)
        return Response(status=200)

    @action(detail=True, methods=['get'])
    def get_partners(self, request, pk):
        try:
            group_id_list = (super().get_queryset()
                .filter(user_id=pk)
                .values_list('group_id', flat=True))

            user_id_list = (super().get_queryset()
                .filter(group_id__in=group_id_list)
                .values_list('user_id', flat=True))

            res = (UserGroup.objects
                .select_related('user')
                .filter(user_id__in=user_id_list)
                .values(
                    'user__first_name', 
                    'user__last_name',
                    'user__phone_number',
                    'user__email',
                    'user__id')
                .distinct())

            return Response(res);
        except: 
            return Response(status=400)
        return Response(status=200)


class DocumentViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.ADMIN, UserType.USER],
                       UserPermission.NO_PERMISSIONS),
    ]),)
    serializer_class = DocumentSerializer
    queryset = Document.objects.all()


class AgencyViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    serializer_class = AgencySerializer
    queryset = Agency.objects.all()


class LoginView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        username = request.data.get('email')
        password = request.data.get('password')
        user = authenticate(email=username, password=password)
        if not user:
            return JsonResponse({'error': 'Invalid username or password'}, status=status.HTTP_401_UNAUTHORIZED)

        if not user.is_active:
            raise JsonResponse({'error': 'User is disabled'}, status=status.HTTP_401_UNAUTHORIZED)

        login(request, user)

        return response.Response()


class LogoutView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        logout(request)
        return response.Response()


class ReportsView(viewsets.ViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.USER], ['GET']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
    ]),)

    def list(self, request):
        raise Http404

    def to_csv_response(self, columns, rows, csv_name):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % csv_name

        writer = csv.writer(response)
        writer.writerow(columns)
        for row in rows:
            writer.writerow(row)
        return response
    
    def to_json_rows(self, columns, rows):
        return [dict(zip(columns, row)) for row in rows]
    
    def to_json_columns(self, columns):
        return map(lambda column: {'title': column, 'dataIndex': column, 'key': column}, columns)

    @action(detail=False, methods=['get'])
    def participant_by_day(self, request):
        try:
            date_from = forms.DateField().clean(self.request.query_params.get('date_from'))
            date_to = forms.DateField().clean(self.request.query_params.get('date_to'))
            agency_id = self.request.user.agency_id
            response_type = self.request.query_params.get('response_type', 'json')
        except: 
            raise

        columns, rows = reports.participant_by_day(agency_id, date_from, date_to)
        if response_type == 'json':
            json_response = {}
            json_response['columns'] = self.to_json_columns(columns)
            json_response['rows'] = self.to_json_rows(columns, rows)
            return Response(json_response, status=200)
        else:
            return self.to_csv_response(columns, rows, 'participant_by_day')

    @action(detail=False, methods=['get'])
    def participant_by_month(self, request):
        try:
            date_from = forms.DateField().clean(self.request.query_params.get('date_from'))
            date_to = forms.DateField().clean(self.request.query_params.get('date_to'))
            agency_id = self.request.user.agency_id
        except:
            raise

        columns, rows = reports.participant_by_month(agency_id, date_from, date_to)
        return self.to_csv_response(columns, rows, 'participant_by_month')

    @action(detail=False, methods=['get'])
    def coaches_by_day(self, request):
        try:
            date_from = forms.DateField().clean(self.request.query_params.get('date_from'))
            date_to = forms.DateField().clean(self.request.query_params.get('date_to'))
            agency_id = self.request.user.agency_id
            response_type = self.request.query_params.get('response_type', 'json')
        except: 
            raise

        columns, rows = reports.coaches_by_day(agency_id, date_from, date_to)

        if response_type == 'json':
            json_response = {}
            json_response['columns'] = self.to_json_columns(columns)
            json_response['rows'] = self.to_json_rows(columns, rows)
            return Response(json_response, status=200)
        else:
            return self.to_csv_response(columns, rows, 'coaches_by_day')

    @action(detail=False, methods=['get'])
    def all_schedules_by_date(self, request):
        try:
            date = forms.DateField().clean(self.request.query_params.get('date'))
            agency_id = self.request.user.agency_id
            user_type = self.request.query_params.get('userType')
            response_type = self.request.query_params.get('response_type', 'json')
        except: 
            raise

        columns, rows = reports.all_schedules_by_date(agency_id, date, user_type)

        json_response = {}
        json_response['columns'] = self.to_json_columns(columns)
        json_response['rows'] = self.to_json_rows(columns, rows)
        return Response(json_response, status=200)