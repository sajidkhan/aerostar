from aerostar.models import Setting, UserType, Address, Agency
from utils.test_util import RestAPITestCase


class TestsSchedulesViewsTestCase(RestAPITestCase):
    fixtures = ['aerostar/fixtures/user_types_fixtures.json']

    def test_get_user_with_type(self):
        user_type = UserType.objects.last()
        self.user.user_type = user_type
        self.user.save()

        response = self.client.get(f'/api/v1/aerostar/user/{str(self.user.id)}/?user_type__type={user_type.type}')
        self.assertMatchSnapshot(response)

    def test_get_current_user_with_type(self):
        user_type = UserType.objects.last()
        self.user.user_type = user_type
        self.user.save()

        response = self.client.get(f'/api/v1/aerostar/current_user/')
        self.assertMatchSnapshot(response)

    def test_get_all_settings(self):
        response = self.client.get('/api/v1/aerostar/settings/')
        self.assertMatchSnapshot(response)

    def test_get_setting(self):
        setting = Setting.objects.create(user=self.user,
                                         days_json={},
                                         time_json={})

        response = self.client.get(f'/api/v1/aerostar/settings/{str(setting.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_setting(self):
        response = self.client.post(f'/api/v1/aerostar/settings/',
                                    data={'days_json': {}, 'user': str(self.user.id), 'time_json': {}},
                                    format='json')
        self.assertMatchSnapshot(response)

    def test_put_setting_when_not_found(self):
        response = self.client.put('/api/v1/aerostar/settings/999/',
                                   data={'days_json': {}, 'user': str(self.user.id), 'time_json': {}},
                                   format='json')
        self.assertMatchSnapshot(response)

    def test_put_setting_when_request_invalid(self):
        setting = Setting.objects.create(user=self.user,
                                         days_json={},
                                         time_json={})
        response = self.client.put(f'/api/v1/aerostar/settings/{setting.id}/',
                                   data={},
                                   format='json')
        self.assertMatchSnapshot(response)

    def test_put_setting_when_request_is_successful(self):
        setting = Setting.objects.create(user=self.user,
                                         days_json={},
                                         time_json={})
        response = self.client.put(f'/api/v1/aerostar/settings/{setting.id}/',
                                   data={'days_json': {}, 'user': str(self.user.id), 'time_json': {}},
                                   format='json')
        self.assertMatchSnapshot(response)

    def test_delete_setting(self):
        setting = Setting.objects.create(user=self.user,
                                         days_json={},
                                         time_json={})

        response = self.client.delete(f'/api/v1/aerostar/settings/{str(setting.id)}/')
        self.assertMatchSnapshot(response)

    def test_get_all_user_types(self):
        response = self.client.get('/api/v1/aerostar/user_types/')
        self.assertMatchSnapshot(response)

    def test_get_user_type(self):
        user_type = UserType.objects.last()

        response = self.client.get(f'/api/v1/aerostar/user_types/{str(user_type.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_user_type(self):
        response = self.client.post(f'/api/v1/aerostar/user_types/',
                                    data={'user_type': str(UserType.USER)},
                                    format='json')
        self.assertMatchSnapshot(response)

    def test_put_user_type(self):
        user_type = UserType.objects.last()
        response = self.client.put(f'/api/v1/aerostar/user_types/{user_type.id}/',
                                   data={'user_type': str(UserType.USER)},
                                   format='json')
        self.assertMatchSnapshot(response)

    def test_delete_user_type(self):
        user_type = UserType.objects.last()

        response = self.client.delete(f'/api/v1/aerostar/user_types/{user_type.id}/')
        self.assertMatchSnapshot(response)

    def test_get_all_addresses(self):
        response = self.client.get('/api/v1/aerostar/addresses/')
        self.assertMatchSnapshot(response)

    def test_get_address(self):
        address = Address.objects.create(user=self.user, address='add1', address_two='address_two',
                                         city='city', state='state', zip='zip', agency=self.agency)

        response = self.client.get(f'/api/v1/aerostar/addresses/{str(address.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_address(self):
        response = self.client.post(f'/api/v1/aerostar/addresses/',
                                    data={'user': str(self.user.id), 'address': 'add1', 'address_two': 'address_two',
                                          'city': 'city', 'state': 'state', 'zip': 'zip',
                                          'agency': str(self.agency.id)},
                                    format='json')
        self.assertMatchSnapshot(response)

    def test_put_address(self):
        address = Address.objects.create(user=self.user, address='add1', address_two='address_two',
                                         city='city', state='state', zip='zip', agency=self.agency)
        response = self.client.put(f'/api/v1/aerostar/addresses/{address.id}/',
                                   data={'user': str(self.user.id), 'address': 'add2', 'address_two': 'address_two',
                                         'city': 'city', 'state': 'state', 'zip': 'zip', 'agency': str(self.agency.id)},
                                   format='json')
        self.assertMatchSnapshot(response)

    def test_delete_address(self):
        address = Address.objects.create(user=self.user, address='add1', address_two='address_two',
                                         city='city', state='state', zip='zip', agency=self.agency)

        response = self.client.delete(f'/api/v1/aerostar/addresses/{address.id}/')
        self.assertMatchSnapshot(response)

    def test_get_all_agencies(self):
        response = self.client.get('/api/v1/aerostar/agencies/')
        self.assertMatchSnapshot(response)

    def test_get_agency(self):
        agency = Agency.objects.create(user=self.user, name='foo')

        response = self.client.get(f'/api/v1/aerostar/agencies/{str(agency.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_agency(self):
        response = self.client.post(f'/api/v1/aerostar/agencies/',
                                    data={'name': 'foo'},
                                    format='json')
        self.assertMatchSnapshot(response)

    def test_put_agency(self):
        agency = Agency.objects.create(name='foo')
        response = self.client.put(f'/api/v1/aerostar/agencies/{agency.id}/',
                                   data={'name': 'foo2'},
                                   format='json')
        self.assertMatchSnapshot(response)

    #
    def test_delete_agency(self):
        agency = Agency.objects.create(name='foo')

        response = self.client.delete(f'/api/v1/aerostar/agencies/{agency.id}/')
        self.assertMatchSnapshot(response)

# TODO: Document, Program, Restriction, Skill, UserSkill, and LocationSkill API unit tests
