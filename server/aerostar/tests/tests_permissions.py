from django.test import TestCase
from aerostar.permissions import UserPermissionFactory, UserPermission
from aerostar.models import UserType

class UserTypeMock:
    def __init__(self):
        self.id = UserType.COACH

class UserMock:
    def __init__(self):
        self.is_authenticated = True
        self.user_type = UserTypeMock()

class RequestMock:
    def __init__(self):
        self.user = UserMock()
        self.method = "GET"

class TestsUserPermissionFactory(TestCase):
    def test_has_permission_when_permission_has_coach_user_role(self):
        subject = UserPermissionFactory([UserPermission([UserType.COACH, UserType.TRANSPORTER], ['GET', 'HEAD', 'PUT', 'POST', 'OPTIONS'])],)
        request_mock = RequestMock()
        self.assertTrue(subject.has_permission(request_mock, None))

    def test_has_permission_when_permission_has_no_coach_user_role(self):
        subject = UserPermissionFactory([UserPermission([UserType.PARTICIPANT], ['GET', 'HEAD', 'PUT', 'POST', 'OPTIONS'])],)
        request_mock = RequestMock()
        self.assertFalse(subject.has_permission(request_mock, None))
