from django.test import TestCase
from aerostar.models import AddressModel


class TestsAerostarModelsTestCase(TestCase):
    def test_address_google_map_url_when_no_address_two(self):
        address = AddressModel(address='700 Van Ness Ave', city='Fresno', state='CA', zip='93721')
        google_map_url = address.google_map_url()
        self.assertEqual(google_map_url,
                         'https://www.google.com/maps/dir/?api=1&destination=700%20Van%20Ness%20Ave%20Fresno%2C%20CA%2093721')

    def test_address_google_map_url_when_address_two(self):
        address = AddressModel(address='700 Van Ness Ave', address_two='room 100', city='Fresno', state='CA',
                               zip='93721')
        google_map_url = address.google_map_url()
        self.assertEqual(google_map_url,
                         'https://www.google.com/maps/dir/?api=1&destination=700%20Van%20Ness%20Ave%20room%20100%20Fresno%2C%20CA%2093721')
