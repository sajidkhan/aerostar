from io import StringIO
from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase
from aerostar.models import UserType


class CreateAerostarSuperUserTest(TestCase):
    def test_command_output_when_no_usertype_admin(self):
        out = StringIO()
        msg = 'UserType ADMIN does not exist. Try running python manage.py loaddata aerostar/fixtures/user_types_fixtures first.'
        with self.assertRaisesMessage(CommandError, msg):
            call_command('createaerostarsuperuser', 'email@email.com', 'firstname', 'lastname', 'foopass',
                         stdout=out)
            
    def test_command_output_when_usertype_admin_exists(self):
        out = StringIO()
        UserType(type='Admin').save()
        msg = 'Successfully created User'
        call_command('createaerostarsuperuser', 'email@email.com', 'firstname', 'lastname', 'foopass', stdout=out)
        self.assertIn(msg, out.getvalue())
