from django.apps import AppConfig


class AerostarConfig(AppConfig):
    name = 'aerostar'
