from import_export import fields, resources, widgets
from aerostar.models import UserType, Agency, Location, Skill, User, UserType, Program, Address, Document, UserSkill, Skill,LocationSkill
from import_export.widgets import ForeignKeyWidget


# Import/Export Resources for admin users.
class LocationResource(resources.ModelResource):
    class Meta:
        model = Location
        fields = ('id','name', 'address', 'address_two', 'city', 'state', 'zip', 'agency')


class SkillResource(resources.ModelResource):
    class Meta:
        model = Skill
        fields = ('id', 'agency', 'skill', 'icon',)


class ProgramResource(resources.ModelResource):
    class Meta:
        model = Program

#Widget for a ForeignKey field which looks up a related model using “natural keys” in both export and import.
#Clean : Returns an appropriate Python object for an imported value.
class UserWidget(widgets.ForeignKeyWidget):
    def clean(self, value, row=None, *args, **kwargs):     
        if value:
            return User.objects.get(id=value)
        else:
            obj, _ = User.objects.get_or_create(
                last_name= row.get('last_name'),
                first_name= row.get('first_name'),
                email= row.get('email'),
                username= row.get('username'),
                password= row.get('password'),
                uci_number= row.get('uci_number'),
                agency = Agency.objects.get(id = row.get('agency')),
                user_type = UserType.objects.get(id = row.get('user_type'))          
            )
        return obj

class AddressResource(resources.ModelResource):
    user = fields.Field(
        column_name='user',
        attribute='user',
        widget=UserWidget(User, 'id'))
    class Meta:
        model = Address
        fields = ('id', 'address', 'address_two', 'city', 'state', 'zip', 'agency', 'user')

class DocumentResource(resources.ModelResource):
    class Meta:
        model = Document

class UserSkillResource(resources.ModelResource):
    class Meta:
        model = UserSkill

class LocationSkillResource(resources.ModelResource):
    class Meta:
        model = LocationSkill


# TODO: All selected widgets do not work via: https://github.com/django-import-export/django-import-export/issues/1123
# For now, we will have to do this sort of thing on the csv side. Once this is fixed, we should import this back in!
class UserResource(resources.ModelResource):
    # user_type = fields.Field(
    #     column_name='user_type',
    #     attribute='user_type',
    #     widget=ForeignKeyWidget(UserType, 'id'))

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'is_active', 'email', 'username', 'password', 'phone_number','uci_number')
