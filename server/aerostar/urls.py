from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import UserViewSet, CurrentUserViewSet, SettingViewSet, UserTypeViewSet, AddressViewSet, LocationViewSet, \
    RestrictionTypeViewSet, LocationSkillViewSet, SkillViewSet, UserSkillViewSet, DocumentViewSet, ProgramViewSet, \
    CoachViewSet, ParticipantViewSet, LoginView, LogoutView, AgencyViewSet, UserAdminViewSet, ReportsView, \
    GroupViewSet, UserGroupViewSet 

router = routers.DefaultRouter()
router.register(r'user', UserViewSet, basename='user')
router.register(r'user_admin', UserAdminViewSet, basename='user_admin')
router.register(r'coach', CoachViewSet, basename='coach')
router.register(r'participant', ParticipantViewSet, basename='participant')
router.register(r'current_user', CurrentUserViewSet, basename='current_user')
router.register(r'settings', SettingViewSet, basename='setting')
router.register(r'user_types', UserTypeViewSet, basename='user_type')
router.register(r'program', ProgramViewSet, basename='program')
router.register(r'addresses', AddressViewSet, basename='address')
router.register(r'locations', LocationViewSet, basename='location')
router.register(r'location_skills', LocationSkillViewSet, basename='location_skill')
router.register(r'skills', SkillViewSet, basename='skill')
router.register(r'user_skills', UserSkillViewSet, basename='user_skill')
router.register(r'group', GroupViewSet, basename='group')
router.register(r'user_groups', UserGroupViewSet, basename='user_group')
router.register(r'documents', DocumentViewSet, basename='document')
router.register(r'restriction_types', RestrictionTypeViewSet, basename='restriction_type')
router.register(r'agencies', AgencyViewSet, basename='agency')
router.register(r'reports', ReportsView, basename='reports')

urlpatterns = [
    url(r'login', LoginView.as_view(), name="login"),
    url(r'logout', LogoutView.as_view(), name="logout"),
    path('', include(router.urls)),
]
