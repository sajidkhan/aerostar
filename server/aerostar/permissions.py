from rest_framework import permissions


class UserPermission:
    ALL_PERMISSIONS = ['GET', 'HEAD', 'PUT', 'POST', 'PATCH', 'DELETE', 'OPTIONS']
    NO_PERMISSIONS = []

    def __init__(self, user_roles, allowed_methods):
        self.user_roles = user_roles
        self.allowed_methods = allowed_methods

"""
Only certain users have access to certain API endpoints.
UserPermissionFactory allows us to assign multiple user roles to multiple collections of permissions.
How to use:
from aerostar.permissions import UserPermissionFactory, UserPermission
from functools import partial

class FooViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER], ['GET', 'HEAD', 'PUT', 'POST', 'OPTIONS']),
        UserPermission([UserType.ADMIN], UserPermission.ALL_PERMISSIONS),
        UserPermission([UserType.PARTICIPANT], ['GET', 'HEAD']),
    ]),)
"""
class UserPermissionFactory(permissions.BasePermission):
    def __init__(self, user_permissions: [UserPermission]):
        super().__init__()
        self.user_permissions = user_permissions

    def has_permission(self, request, view):
        # If the request has no user (unauthenticated) or no User Type (no admin has assigned a user type)
        if not request.user or not hasattr(request.user, 'user_type') or not request.user.is_authenticated:
            return False

        for user_permission in self.user_permissions:
            if request.user.user_type.id in user_permission.user_roles:
                return request.method in user_permission.allowed_methods

        # If a user type is not defined, return False
        return False
