from django.db import transaction
from datetime import datetime
from rest_flex_fields import FlexFieldsModelSerializer
from rest_framework import serializers
from django.utils import timezone
from datetime import timedelta
from transportation.models import Vehicle, TransportationType
from .mixins import AddAgencyMixin
from .models import User, Setting, UserType, Address, Location, RestrictionType, LocationSkill, Skill, UserSkill, \
    Document, Agency, Program, Group, UserGroup
from schedules.models import ScheduleType, ScheduledCoach
from transportation.serializers import TransportationTypeSerializer, ZoneSerializer, VehicleSerializer
from schedules.copy_from_master import DATE_MAP as MASTER_SCHEDULE_DATES


class SettingSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = Setting
        fields = '__all__'


class ProgramSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = Program
        fields = '__all__'


class AgencySerializer(serializers.ModelSerializer):
    programs = ProgramSerializer(read_only=True, many=True)
    transportation_types = TransportationTypeSerializer(read_only=True, many=True)
    zones = ZoneSerializer(read_only=True, many=True)

    class Meta:
        model = Agency
        exclude = ['subdomain']

    @transaction.atomic
    def create(self, validated_data):
        agency = Agency.objects.create(**validated_data)

        # Default Program
        Program.objects.create(agency=agency, name='Default')
        # Default transportation Types (Default FHS and Standard per agency).
        TransportationType.objects.bulk_create(
            [TransportationType(agency=agency, reimbursement=TransportationType.FMS, name=f"FMS"),
             TransportationType(agency=agency, reimbursement=TransportationType.Standard,
                                name=f"Standard")]
        )

        ScheduleType.objects.bulk_create([
            ScheduleType(agency=agency,
                         type='start',
                         start_time="08:00:00",
                         end_time="10:00:00"),
            ScheduleType(agency=agency,
                         type='event',
                         start_time="10:00:00",
                         end_time="12:00:00"),
            ScheduleType(agency=agency,
                         type='end',
                         start_time="12:00:00",
                         end_time="14:00:00")
        ])

        return agency


class UserTypeSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = UserType
        fields = '__all__'


class CurrentUserSerializer(AddAgencyMixin, serializers.ModelSerializer):
    user_type = serializers.CharField(source='user_type.type', read_only=True)
    agency = AgencySerializer(read_only=True)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'user_type', 'program', 'agency']


class AddressSerializer(AddAgencyMixin, FlexFieldsModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Address
        fields = '__all__'
        expandable_fields = {
            'google_map_url': (serializers.CharField, {'read_only': True})
        }
        extra_kwargs = {'user': {'required': False}, 'agency': {'required': False}}


class UserTypePKField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return UserType.objects.filter(pk__in=[UserType.USER, UserType.ADMIN])


class UserSerializerAdmin(serializers.ModelSerializer):
    user_type = UserTypePKField()
    agency = serializers.PrimaryKeyRelatedField(queryset=Agency.objects.all())

    def validate(self, data):
        validated_data = super(UserSerializerAdmin, self).validate(data)

        if validated_data['program'] and validated_data['program'].agency_id != validated_data['agency'].id:
            raise serializers.ValidationError("Program must be in the same agency")

        if not validated_data['program'] and validated_data['user_type'].id == UserType.USER:
            raise serializers.ValidationError("User must have a program")

        return validated_data

    class Meta:
        model = User
        fields = ['id', 'email', 'first_name', 'last_name', 'user_type', 'agency', 'program']
        extra_kwargs = {'email': {'required': True, 'allow_blank': False},
                        'first_name': {'required': True, 'allow_blank': False},
                        'last_name': {'required': True, 'allow_blank': False},
                        'user_type': {'required': True, 'allow_blank': False},
                        'agency': {'required': True, 'allow_blank': False},
                        }


class ScheduledCoachUserSerializer(AddAgencyMixin, serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'program']


class UserSerializer(AddAgencyMixin, serializers.ModelSerializer):
    user_type = serializers.PrimaryKeyRelatedField(queryset=UserType.objects.all())
    agency = serializers.PrimaryKeyRelatedField(queryset=Agency.objects.all())
    addresses = AddressSerializer(many=True, required=False)
    vehicles = VehicleSerializer(many=True, required=False)
    background_check = serializers.SerializerMethodField()
    immunization_records = serializers.SerializerMethodField()
    background_check_upload = serializers.ListSerializer(child=serializers.FileField(), write_only=True, required=False)
    immunization_records_upload = serializers.ListSerializer(child=serializers.FileField(), write_only=True,
                                                             required=False)

    def get_background_check(self, obj):
        items = Document.objects.filter(user=obj, type=Document.BACKGROUND)
        serializer = DocumentSerializer(instance=items, many=True)
        return serializer.data

    def get_immunization_records(self, obj):
        items = Document.objects.filter(user=obj, type=Document.IMMUNIZATION)
        serializer = DocumentSerializer(instance=items, many=True)
        return serializer.data

    def pop(self, validated_data, key):
        try:
            return validated_data.pop(key)
        except KeyError:
            return []

    @transaction.atomic
    def update(self, instance, validated_data):
        vehicles_data = self.pop(validated_data, 'vehicles')
        background_check_data = self.pop(validated_data, 'background_check_upload')
        immunization_records_data = self.pop(validated_data, 'immunization_records_upload')
        addresses_data = self.pop(validated_data, 'addresses')
        if(instance.deleted != None and validated_data.get('deleted') == None):            
            # on reactivating coach we need to update coachschedule deleted to null.
            ScheduledCoach.all_objects.select_related('schedule').filter(                
                agency=instance.agency,
                user=instance.id,deleted__isnull=False).update(deleted=None)            

        for address_data in addresses_data[:1]:
            if 'id' in address_data:
                if Address.objects.filter(id=address_data['id'], user=instance, agency=instance.agency).exists():
                    Address.objects.filter(id=address_data['id']).update(**address_data)
                else:
                    raise serializers.ValidationError("Unable to find Address")
            else:
                if Address.objects.filter(user=instance, agency=instance.agency).exists():
                    # User already has an address so shouldn't pass a new one
                    raise serializers.ValidationError("Unable to find Address")
                else:
                    Address.objects.create(user=instance, **address_data)

        for document in background_check_data:
            Document.objects.create(agency=instance.agency, user=instance, s_three_location=document,
                                    type=Document.BACKGROUND)

        for document in immunization_records_data:
            Document.objects.create(agency=instance.agency, user=instance, s_three_location=document,
                                    type=Document.IMMUNIZATION)

        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.email = validated_data.get('email', instance.email)
        instance.user_type = validated_data.get('user_type', instance.user_type)
        instance.nickname = validated_data.get('nickname', instance.nickname)
        instance.uci_number = validated_data.get('uci_number', instance.uci_number)
        instance.zone = validated_data.get('zone', instance.zone)
        instance.transportation_type = validated_data.get('transportation_type', instance.transportation_type)
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.note = validated_data.get('note', instance.note)
        instance.work_days = validated_data.get('work_days', instance.work_days)
        instance.work_hours_start = validated_data.get('work_hours_start', instance.work_hours_start)
        instance.work_hours_end = validated_data.get('work_hours_end', instance.work_hours_end)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        program_check = validated_data.get('program', instance.program)
        instance.send_notification_email = validated_data.get('send_notification_email', instance.send_notification_email)
        if isinstance(program_check, list):
            instance.program.set(program_check)
        else:
            pass
        instance.save()

        return instance

    @transaction.atomic
    def create(self, validated_data):
        background_check_data = self.pop(validated_data, 'background_check_upload')
        immunization_records_data = self.pop(validated_data, 'immunization_records_upload')
        addresses_data = self.pop(validated_data, 'addresses')

        # Since Program is a multi-to-multi relationship, we need to remove it from the 
        # validated data before we create the user to avoid an error (even if program is empty)
        if 'program' in validated_data and validated_data['program']:
            program_check = validated_data['program']
            validated_data.pop('program', None)
            user = User.objects.create(**validated_data)
            user.program.set(program_check)
        else:
            validated_data.pop('program', None)
            user = User.objects.create(**validated_data)

        for address_data in addresses_data[:1]:
            Address.objects.create(user=user, **address_data)

        for document in background_check_data:
            Document.objects.create(agency=validated_data['agency'], user=user, s_three_location=document,
                                    type=Document.BACKGROUND)
        for document in immunization_records_data:
            Document.objects.create(agency=validated_data['agency'], user=user, s_three_location=document,
                                    type=Document.IMMUNIZATION)

        return user

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'user_type', 'transportation_type', 'agency', 'addresses',
                  'email', 'phone_number', 'nickname', 'uci_number', 'work_days', 'work_hours_start',
                  'work_hours_end', 'program', 'zone', 'note', 'vehicles', 'background_check_upload',
                  'immunization_records_upload',
                  'background_check', 'immunization_records', 'is_active','deleted','send_notification_email']
        extra_kwargs = {'vehicles': {'required': False}}


class LocationSerializer(AddAgencyMixin, serializers.ModelSerializer):
    google_map_url = serializers.CharField(read_only=True)

    class Meta:
        model = Location
        fields = '__all__'


class RestrictionTypeSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = RestrictionType
        fields = '__all__'


class LocationSkillSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = LocationSkill
        fields = '__all__'


class SkillSerializer(AddAgencyMixin, serializers.ModelSerializer):
    name = serializers.CharField(source='skill')

    class Meta:
        model = Skill
        fields = '__all__'


class UserSkillSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = UserSkill
        fields = '__all__'


class GroupSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'group', 'agency']


class UserGroupSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = UserGroup
        fields = ['id', 'group', 'user']


class DocumentSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = '__all__'
