from django.core.management.base import BaseCommand, CommandError
from aerostar.models import User, UserType, Agency


class Command(BaseCommand):
    help = 'Creates Aerostar Super User. Auto-creates Shift3 agency if one is not already created.'

    def add_arguments(self, parser):
        parser.add_argument('email', nargs='+', type=str)
        parser.add_argument('firstname', nargs='+', type=str)
        parser.add_argument('lastname', nargs='+', type=str)
        parser.add_argument('password', nargs='+', type=str)

    def handle(self, *args, **options):
        email = options['email'][0]
        firstname = options['firstname'][0]
        lastname = options['lastname'][0]
        password = options['password'][0]
        agency, created_agency = Agency.objects.get_or_create(name='Shift3')
        try:
            usertype = UserType.objects.get(type='Admin')
        except UserType.DoesNotExist:
            raise CommandError(
                'UserType ADMIN does not exist. ' +
                'Try running python manage.py loaddata aerostar/fixtures/user_types_fixtures first.')

        try:
            user = User(email=email, first_name=firstname, last_name=lastname, agency=agency,
                        user_type=usertype)
            user.set_password(password)
            user.is_superuser = True
            user.is_staff = True
            user.save()
        except Exception as ex:
            raise CommandError(f'Error in creating user: {str(ex)}')

        self.stdout.write(f'Successfully created User {email}: User id {user.id}')
