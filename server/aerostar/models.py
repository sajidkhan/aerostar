from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from safedelete import SOFT_DELETE_CASCADE
from safedelete.managers import SafeDeleteManager
from safedelete.models import SafeDeleteModel
from simple_history.models import HistoricalRecords
from django.contrib.postgres.fields import JSONField, ArrayField
import urllib.parse
from server.storage_backends import PrivateMediaStorage


class BaseModel(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        # using .format for backward compatibility
        verbose_name = 'VERBOSE NOT SET'.format(__name__)
        verbose_name_plural = 'VERBOSE PLURAL NOT SET'.format(__name__)
        abstract = True



class AddressModel(models.Model):
    address = models.CharField(max_length=30,blank=True,null=True)
    address_two = models.CharField(max_length=30, blank=True, null=True)
    city = models.CharField(max_length=30,blank=True,null=True)
    state = models.CharField(max_length=30,blank=True,null=True)
    zip = models.CharField(max_length=30,blank=True,null=True)

    def full_address(self):
        if not self.address:
            return ''
        full_address = self.address
        if self.address_two:
            full_address += f' {self.address_two}'
        full_address = f'{full_address} {self.city}, {self.state} {self.zip}'

        return f'{full_address}'

    # Based on https://developers.google.com/maps/documentation/urls/guide
    # Generates a valid google map url from address fields
    def google_map_url(self):
        if not self.address:
            return ''

        map_url = 'https://www.google.com/maps/dir/?api=1&destination='
        full_address = self.address
        if self.address_two:
            full_address += f' {self.address_two}'
        composed_query = f'{full_address} {self.city}, {self.state} {self.zip}'

        # Encoding must be done to special chars: https://developers.google.com/maps/documentation/urls/url-encoding
        encoded_query = urllib.parse.quote(composed_query)

        return f'{map_url}{encoded_query}'

    class Meta:
        abstract = True


class Agency(BaseModel):
    history = HistoricalRecords()
    name = models.CharField(max_length=100)
    subdomain = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = 'Agency'
        verbose_name_plural = 'Agencies'

    def __str__(self):
        return f'{self.name}'


class Program(BaseModel):
    history = HistoricalRecords()
    name = models.CharField(max_length=100)
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE, blank=True, null=True, related_name='programs')

    class Meta:
        verbose_name = 'Program'
        verbose_name_plural = 'Programs'

    def __str__(self):
        return f'{self.id}: {self.name} - {self.agency}'


class UserType(BaseModel):
    PARTICIPANT = 1
    COACH = 2
    USER = 3
    ADMIN = 4
    TRANSPORTER = 5
    TYPE_CHOICES = (
        ('Participant', 'Participant'),
        ('Coach', 'Coach'),
        ('User', 'User'),
        ('Admin', 'Admin'),
        ('Transporter', 'Transporter'),
    )
    type = models.CharField(max_length=30, default=COACH, choices=TYPE_CHOICES)

    class Meta:
        verbose_name = 'UserType'
        verbose_name_plural = 'UserTypes'

    def __str__(self):
        return f'{self.id}: {self.type}'


# The SafeDeleteManager is needed to properly hide soft deleted users
class SafeDeleteUserManager(UserManager, SafeDeleteManager):
    pass


class User(AbstractUser, BaseModel):
    history = HistoricalRecords()
    REQUIRED_FIELDS = ["password", "first_name", "last_name", "user_type", "agency"]
    objects = SafeDeleteUserManager()
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        blank=True,
        null=True
    )
    USERNAME_FIELD = "email"
    username = models.CharField(error_messages={'unique': 'A user with that username already exists.'},
                                help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.',
                                max_length=150,
                                unique=True, validators=[UnicodeUsernameValidator()],
                                verbose_name='username', blank=True, null=True)
    password = models.CharField(max_length=128, verbose_name='password', blank=True, null=True)
    phone_number = models.CharField(max_length=30, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    nickname = models.CharField(max_length=30, blank=True, null=True)
    uci_number = models.CharField(max_length=30, blank=True, null=True)
    zone = models.ForeignKey("transportation.Zone", on_delete=models.SET_NULL, blank=True, null=True)
    work_days = ArrayField(models.CharField(max_length=10, blank=True, null=True), blank=True, null=True)
    work_hours_start = models.TimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    work_hours_end = models.TimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE, blank=True, null=True)
    user_type = models.ForeignKey(UserType, on_delete=models.CASCADE, blank=True, null=True)
    transportation_type = models.ForeignKey("transportation.TransportationType", on_delete=models.CASCADE, blank=True,
                                            null=True)
    program = models.ManyToManyField(Program, related_name='users', blank=True)
    send_notification_email = models.BooleanField(default=True)

    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return f'{self.first_name} {self.last_name} - {self.agency}'


class Location(BaseModel, AddressModel):
    history = HistoricalRecords()
    HOME = 1
    MAIN_OFFICE = 2
    name = models.CharField(max_length=100)
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE, blank=True, null=True)
    phone_number = models.CharField(max_length=30)
    note = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = 'Location'
        verbose_name_plural = 'Locations'

    def __str__(self):
        return f'{self.id}: {self.name}'


class Address(BaseModel, AddressModel):
    history = HistoricalRecords()
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE, related_name='addresses')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='addresses')

    class Meta:
        verbose_name = 'Address'
        verbose_name_plural = 'Addresses'

    def __str__(self):
        return f'{self.id}: {self.address} {self.address_two} {self.city} {self.state}'


class Skill(BaseModel):
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE, blank=True, null=True)
    skill = models.CharField(max_length=100)
    icon = models.CharField(max_length=30)

    class Meta:
        verbose_name = 'Skill'
        verbose_name_plural = 'Skills'

    def __str__(self):
        return f'{self.id}: {self.skill}'


class Setting(BaseModel):
    history = HistoricalRecords()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    days_json = JSONField()
    time_json = JSONField()

    class Meta:
        verbose_name = 'Setting'
        verbose_name_plural = 'Settings'

    def __str__(self):
        return f'{self.id}: {self.user}'


class RestrictionType(BaseModel):
    name = models.CharField(max_length=30)

    class Meta:
        verbose_name = 'RestrictionType'
        verbose_name_plural = 'RestrictionTypes'

    def __str__(self):
        return f'{self.id}: {self.name}'


class LocationSkill(BaseModel):
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    restriction_type = models.ForeignKey(RestrictionType, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'LocationSkill'
        verbose_name_plural = 'LocationSkills'

    def __str__(self):
        return f'{self.id}: {self.location} {self.skill}'


class UserSkill(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'UserSkill'
        verbose_name_plural = 'UserSkills'

    def __str__(self):
        return f'{self.id}: {self.user} {self.skill}'

class Group(models.Model):
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE, blank=True, null=True)
    group = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'Group'
        verbose_name_plural = 'Groups'

    def __str__(self):
        return f'{self.id}: {self.group}'

class UserGroup(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'UserGroup'
        verbose_name_plural = 'UserGroups'

    def __str__(self):
        return f'{self.id}: {self.user} {self.group}'

class Document(BaseModel):
    history = HistoricalRecords()
    BACKGROUND = 'Background'
    IMMUNIZATION = 'Immunization'
    VEHICLE = 'Vehicle'
    LICENSE = 'License'
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vehicle = models.ForeignKey("transportation.Vehicle", on_delete=models.CASCADE, null=True, blank=True)
    restriction_type = models.ForeignKey(RestrictionType, on_delete=models.CASCADE, null=True, blank=True)
    expiration_date = models.DateField(null=True, blank=True)
    s_three_location = models.FileField(storage=PrivateMediaStorage(), null=True, blank=True)
    TYPE_CHOICES = (
        ('Background', 'Background'),
        ('Immunization', 'Immunization'),
        ('Vehicle', 'Vehicle'),
        ('License', 'License'),
    )
    type = models.CharField(max_length=30, choices=TYPE_CHOICES)

    class Meta:
        verbose_name = 'Document'
        verbose_name_plural = 'Documents'

    def __str__(self):
        return f'{self.id}: {self.s_three_location}'
