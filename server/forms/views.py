from rest_framework import viewsets
from aerostar.permissions import UserPermissionFactory, UserPermission
from functools import partial
from forms.models import Form
from aerostar.models import UserType
from forms.serializers import FormSerializer
from rest_framework.response import Response


class FormViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER], ['GET']),
        UserPermission([UserType.ADMIN, UserType.USER], UserPermission.ALL_PERMISSIONS),
    ]),)

    queryset = Form.objects.all()
    serializer_class = FormSerializer