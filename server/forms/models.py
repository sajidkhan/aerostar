from django.db import models
from aerostar.models import User

class Form(models.Model):
    SURVEY_TYPES = [(item, item) for item in ['COACHES', 'PARTICIPANTS', 'BOTH']]

    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    title = models.CharField(max_length=255)
    url = models.TextField()
    survey_type = models.CharField(choices=SURVEY_TYPES, max_length=255)
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ['created']