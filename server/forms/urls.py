from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers
from .views import FormViewSet

router = routers.DefaultRouter()

router.register(r'', FormViewSet, basename='form')

urlpatterns = [
    path('', include(router.urls)),
]