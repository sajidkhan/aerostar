from django.db import models
from djmoney.models.fields import MoneyField
from simple_history.models import HistoricalRecords

from aerostar.models import BaseModel, Agency, User, Address
from rest_framework import serializers


class TransportationType(BaseModel):
    history = HistoricalRecords()
    FMS = 1
    Standard = 2
    REIMBURSEMENT_CHOICES = (
        (FMS, 'FMS'),
        (Standard, 'Standard'),
    )

    reimbursement = models.IntegerField(default=2, choices=REIMBURSEMENT_CHOICES)
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE,  related_name='transportation_types')
    name = models.CharField(max_length=30)

    class Meta:
        verbose_name = 'TransportationType'
        verbose_name_plural = 'TransportationTypes'

    def __str__(self):
        return f'{self.id}: {self.name} {self.reimbursement}'


class Vehicle(BaseModel):
    history = HistoricalRecords()
    id = serializers.IntegerField(required=False)
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE, related_name='vehicles')
    owned_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='vehicles')
    is_owned_by_agency = models.BooleanField(default=True)
    description = models.TextField(blank=True, null=True)
    make = models.CharField(max_length=30)
    model = models.CharField(max_length=30)
    year = models.CharField(max_length=30)
    color = models.CharField(max_length=30)
    license_number = models.CharField(max_length=30)
    doc_expiration = models.DateField()

    class Meta:
        verbose_name = 'Vehicle'
        verbose_name_plural = 'Vehicles'

    def __str__(self):
        return f'{self.id}: {self.owned_by} {self.description}'


class Zone(BaseModel):
    history = HistoricalRecords()
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE, related_name='zones')
    zone = models.CharField(max_length=30)
    price = MoneyField(max_digits=10, decimal_places=2, null=True, default_currency='USD')

    class Meta:
        verbose_name = 'Zone'
        verbose_name_plural = 'Zones'

    def __str__(self):
        return f'{self.id}: {self.zone} {self.price}'


class PersonZone(BaseModel):
    history = HistoricalRecords()
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'PersonZone'
        verbose_name_plural = 'PersonZones'

    def __str__(self):
        return f'{self.id}: {self.zone} {self.address}'
