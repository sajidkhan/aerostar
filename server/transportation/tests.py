from aerostar.models import Address
from transportation.models import TransportationType, Vehicle, Zone, PersonZone
from moneyed import Money, USD
# from djmoney.money import Money
from utils.test_util import RestAPITestCase


class APITestCase(RestAPITestCase):

    def test_get_all_transportation_types(self):
        """Testing the API for /api/v1/transportation/transportation_types"""
        response = self.client.get('/api/v1/transportation/transportation_types/')
        self.assertMatchSnapshot(response)

    def test_get_transportation_type(self):
        transportation_type = TransportationType.objects.create(reimbursement=TransportationType.FMS,
                                                                agency=self.agency,
                                                                name='foo')

        response = self.client.get(f'/api/v1/transportation/transportation_types/{str(transportation_type.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_transportation_types(self):
        response = self.client.post(f'/api/v1/transportation/transportation_types/',
                                    data={'reimbursement': '1', 'agency': str(self.agency.id), 'name': 'foo'})
        self.assertMatchSnapshot(response)

    def test_put_transportation_type_when_not_found(self):
        response = self.client.put('/api/v1/transportation/transportation_types/999/',
                                   data={'reimbursement': '1', 'agency': str(self.agency.id), 'name': 'foo'})
        self.assertMatchSnapshot(response)

    def test_put_transportation_type_when_request_invalid(self):
        transportation_type = TransportationType.objects.create(reimbursement=TransportationType.FMS,
                                                                agency=self.agency,
                                                                name='foo')
        response = self.client.put(f'/api/v1/transportation/transportation_types/{transportation_type.id}/',
                                   data={'reimbursement': '10', 'agency': str(self.agency.id), 'name': 'foo'})
        self.assertMatchSnapshot(response)

    def test_put_transportation_type_when_request_is_successful(self):
        transportation_type = TransportationType.objects.create(reimbursement=TransportationType.FMS,
                                                                agency=self.agency,
                                                                name='foo')
        response = self.client.put(f'/api/v1/transportation/transportation_types/{transportation_type.id}/',
                                   data={'reimbursement': '1', 'agency': str(self.agency.id), 'name': 'foo'})
        self.assertMatchSnapshot(response)

    def test_delete_event_type(self):
        transportation_type = TransportationType.objects.create(reimbursement=TransportationType.FMS,
                                                                agency=self.agency,
                                                                name='foo')

        response = self.client.delete(f'/api/v1/transportation/transportation_types/{str(transportation_type.id)}/')
        self.assertMatchSnapshot(response)

    def test_get_all_vehicles(self):
        response = self.client.get('/api/v1/transportation/vehicles/')
        self.assertMatchSnapshot(response)

    def create_vehicle(self):
        return Vehicle.objects.create(owned_by=self.user,
                               agency=self.agency,
                               description='foo',
                               make='Ford',
                               model='Mustang',
                               year='1999',
                               color='black',
                               license_number='12345',
                               doc_expiration='2020-12-12',
                               )

    def get_vehicle_data(self):
        return {
            'owned_by': str(self.user.id),
            'agency': str(self.agency.id),
            'description': 'foo',
            'make': 'Ford',
            'model': 'Mustang',
            'year': '1999',
            'color': 'black',
            'license_number': '12345',
            'doc_expiration': '2020-12-12',
            }

    def test_get_vehicle(self):
        vehicle = self.create_vehicle()
        response = self.client.get(f'/api/v1/transportation/vehicles/{str(vehicle.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_vehicle(self):
        response = self.client.post(f'/api/v1/transportation/vehicles/',
                                    data=self.get_vehicle_data())

        self.assertMatchSnapshot(response)

    def test_put_vehicle_when_not_found(self):
        response = self.client.put('/api/v1/transportation/vehicles/999/',
                                   data=self.get_vehicle_data())
        self.assertMatchSnapshot(response)

    def test_put_vehicle_when_request_invalid(self):
        vehicle = self.create_vehicle()
        response = self.client.put(f'/api/v1/transportation/vehicles/{vehicle.id}/',
                                   data={'agency': str(self.agency.id),
                                         'description': 'foo'})
        self.assertMatchSnapshot(response)

    def test_put_vehicle_when_request_is_successful(self):
        vehicle = self.create_vehicle()
        response = self.client.put(f'/api/v1/transportation/vehicles/{vehicle.id}/',
                                   data=self.get_vehicle_data())
        self.assertMatchSnapshot(response)

    def test_delete_vehicle(self):
        vehicle = self.create_vehicle()

        response = self.client.delete(f'/api/v1/transportation/vehicles/{str(vehicle.id)}/')
        self.assertMatchSnapshot(response)

    def test_get_all_zones(self):
        response = self.client.get('/api/v1/transportation/zones/')
        self.assertMatchSnapshot(response)

    def test_get_zone(self):
        zone = Zone.objects.create(agency=self.user.agency,
                                   zone='foo',
                                   price=Money(1, 'USD'))

        response = self.client.get(f'/api/v1/transportation/zones/{str(zone.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_zone(self):
        response = self.client.post(f'/api/v1/transportation/zones/',
                                    data={'price': 1.0,
                                          'price_currency': 'USD',
                                          'agency': str(self.agency.id),
                                          'zone': 'foo'})
        self.assertMatchSnapshot(response)

    def test_put_zone_when_not_found(self):
        response = self.client.put('/api/v1/transportation/zones/999/',
                                   data={'price': 1.0,
                                         'agency': str(self.agency.id),
                                         'zone': 'foo'})
        self.assertMatchSnapshot(response)

    def test_put_zone_when_request_invalid(self):
        zone = Zone.objects.create(agency=self.user.agency,
                                   zone='foo',
                                   price=Money(1, 'USD'))
        response = self.client.put(f'/api/v1/transportation/zones/{zone.id}/',
                                   data={})
        self.assertMatchSnapshot(response)

    def test_put_zone_when_request_is_successful(self):
        zone = Zone.objects.create(agency=self.user.agency,
                                   zone='foo',
                                   price=Money(1, 'USD'))
        response = self.client.put(f'/api/v1/transportation/zones/{zone.id}/',
                                   data={'price': 1,
                                         'agency': str(self.agency.id),
                                         'zone': 'foo'})
        self.assertMatchSnapshot(response)

    def test_delete_zone(self):
        zone = Zone.objects.create(agency=self.user.agency,
                                   zone='foo',
                                   price=Money(1, 'USD'))

        response = self.client.delete(f'/api/v1/transportation/zones/{str(zone.id)}/')
        self.assertMatchSnapshot(response)

    def test_get_all_person_zones(self):
        response = self.client.get('/api/v1/transportation/person_zones/')
        self.assertMatchSnapshot(response)

    def test_get_person_zone(self):
        address = Address.objects.create(user=self.user, address='add1', address_two='address_two',
                                         city='city', state='state', zip='zip', agency=self.agency)
        zone = Zone.objects.create(agency=self.user.agency,
                                   zone='foo',
                                   price=Money(1, 'USD'))

        person_zone = PersonZone.objects.create(zone=zone, address=address)

        response = self.client.get(f'/api/v1/transportation/person_zones/{str(person_zone.id)}/')
        self.assertMatchSnapshot(response)

    def test_post_person_zone(self):
        address = Address.objects.create(user=self.user, address='add1', address_two='address_two',
                                         city='city', state='state', zip='zip', agency=self.agency)
        zone = Zone.objects.create(agency=self.user.agency,
                                   zone='foo',
                                   price=Money(1, 'USD'))

        response = self.client.post(f'/api/v1/transportation/person_zones/',
                                    data={'zone': str(zone.id), 'address': str(address.id)})
        self.assertMatchSnapshot(response)

    def test_put_person_zone_when_request_invalid(self):
        address = Address.objects.create(user=self.user, address='add1', address_two='address_two',
                                         city='city', state='state', zip='zip', agency=self.agency)
        zone = Zone.objects.create(agency=self.user.agency,
                                   zone='foo',
                                   price=Money(1, 'USD'))

        person_zone = PersonZone.objects.create(zone=zone, address=address)

        response = self.client.put(f'/api/v1/transportation/person_zones/{person_zone.id}/',
                                   data={})
        self.assertMatchSnapshot(response)

    def test_put_person_zone_when_request_is_successful(self):
        address = Address.objects.create(user=self.user, address='add1', address_two='address_two',
                                         city='city', state='state', zip='zip', agency=self.agency)
        zone = Zone.objects.create(agency=self.user.agency,
                                   zone='foo',
                                   price=Money(1, 'USD'))

        person_zone = PersonZone.objects.create(zone=zone, address=address)

        response = self.client.put(f'/api/v1/transportation/person_zones/{person_zone.id}/',
                                   data={'zone': str(zone.id), 'address': str(address.id)})
        self.assertMatchSnapshot(response)

    def test_delete_person_zone(self):
        address = Address.objects.create(user=self.user, address='add1', address_two='address_two',
                                         city='city', state='state', zip='zip', agency=self.agency)
        zone = Zone.objects.create(agency=self.user.agency,
                                   zone='foo',
                                   price=Money(1, 'USD'))

        person_zone = PersonZone.objects.create(zone=zone, address=address)

        response = self.client.delete(f'/api/v1/transportation/person_zones/{str(person_zone.id)}/')
        self.assertMatchSnapshot(response)
