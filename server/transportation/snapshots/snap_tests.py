# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import GenericRepr, Snapshot


snapshots = Snapshot()

snapshots['APITestCase::test_get_all_transportation_types 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['APITestCase::test_get_transportation_type 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['APITestCase::test_put_transportation_type_when_not_found 1'] = GenericRepr('<Response status_code=404, "application/json">')

snapshots['APITestCase::test_put_transportation_type_when_request_invalid 1'] = GenericRepr('<Response status_code=400, "application/json">')

snapshots['APITestCase::test_put_transportation_type_when_request_is_successful 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['APITestCase::test_delete_event_type 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_get_all_vehicles 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['APITestCase::test_get_vehicle 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['APITestCase::test_post_vehicle 1'] = GenericRepr('<Response status_code=201, "application/json">')

snapshots['APITestCase::test_put_vehicle_when_not_found 1'] = GenericRepr('<Response status_code=404, "application/json">')

snapshots['APITestCase::test_put_vehicle_when_request_invalid 1'] = GenericRepr('<Response status_code=400, "application/json">')

snapshots['APITestCase::test_put_vehicle_when_request_is_successful 1'] = GenericRepr('<Response status_code=200, "application/json">')

snapshots['APITestCase::test_delete_vehicle 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_get_all_zones 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_put_zone_when_not_found 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_get_zone 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_delete_zone 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_put_zone_when_request_invalid 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_put_zone_when_request_is_successful 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_post_zone 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_get_all_person_zones 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_post_person_zone 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_post_transportation_types 1'] = GenericRepr('<Response status_code=201, "application/json">')

snapshots['APITestCase::test_delete_person_zone 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_put_person_zone_when_request_invalid 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_put_person_zone_when_request_is_successful 1'] = GenericRepr('<Response status_code=403, "application/json">')

snapshots['APITestCase::test_get_person_zone 1'] = GenericRepr('<Response status_code=403, "application/json">')
