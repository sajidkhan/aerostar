from rest_framework import serializers
from django.db import transaction
from aerostar.mixins import AddAgencyMixin
from aerostar.models import Document, User
from transportation.models import TransportationType, Vehicle, Zone, PersonZone


class TransportationTypeSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = TransportationType
        fields = '__all__'


class VehicleSerializer(AddAgencyMixin, serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    vehicle_photo = serializers.SerializerMethodField()
    license_photo = serializers.SerializerMethodField()
    vehicle_photo_upload = serializers.ListSerializer(child=serializers.FileField(), write_only=True, required=False)
    license_photo_upload = serializers.ListSerializer(child=serializers.FileField(), write_only=True, required=False)

    def get_vehicle_photo(self, obj):
        from aerostar.serializers import DocumentSerializer
        items = Document.objects.filter(vehicle=obj, type=Document.VEHICLE)
        serializer = DocumentSerializer(instance=items, many=True)
        return serializer.data

    def get_license_photo(self, obj):
        from aerostar.serializers import DocumentSerializer
        items = Document.objects.filter(vehicle=obj, type=Document.LICENSE)
        serializer = DocumentSerializer(instance=items, many=True)
        return serializer.data

    def pop(self, validated_data, key):
        try:
            return validated_data.pop(key)
        except KeyError:
            return []

    @transaction.atomic
    def create(self, validated_data):
        vehicle_photo_data = self.pop(validated_data, 'vehicle_photo_upload')
        license_photo_data = self.pop(validated_data, 'license_photo_upload')

        vehicle = Vehicle.objects.create(**validated_data)
        if vehicle.owned_by:

            for document in vehicle_photo_data:
                Document.objects.create(agency=vehicle.owned_by.agency, user=vehicle.owned_by,
                                        s_three_location=document,
                                        type=Document.VEHICLE, vehicle=vehicle)

            for document in license_photo_data:
                Document.objects.create(agency=vehicle.owned_by.agency, user=vehicle.owned_by,
                                        s_three_location=document,
                                        type=Document.LICENSE, vehicle=vehicle)

        return vehicle

    @transaction.atomic
    def update(self, instance, validated_data):
        vehicle_photo_data = self.pop(validated_data, 'vehicle_photo_upload')
        license_photo_data = self.pop(validated_data, 'license_photo_upload')

        instance.is_owned_by_agency = validated_data.get('is_owned_by_agency', instance.is_owned_by_agency)
        instance.description = validated_data.get('description', instance.description)
        instance.make = validated_data.get('make', instance.make)
        instance.model = validated_data.get('model', instance.model)
        instance.year = validated_data.get('year', instance.year)
        instance.color = validated_data.get('color', instance.color)
        instance.license_number = validated_data.get('license_number', instance.license_number)
        instance.doc_expiration = validated_data.get('doc_expiration', instance.doc_expiration)
        instance.save()

        if instance.owned_by:
            for document in vehicle_photo_data:
                Document.objects.create(agency=instance.owned_by.agency, user=instance.owned_by,
                                        s_three_location=document,
                                        type=Document.VEHICLE, vehicle=instance)

            for document in license_photo_data:
                Document.objects.create(agency=instance.owned_by.agency, user=instance.owned_by,
                                        s_three_location=document,
                                        type=Document.LICENSE, vehicle=instance)

        return instance

    class Meta:
        model = Vehicle
        fields = '__all__'
        extra_kwargs = {'owned_by': {'required': False}, 'agency': {'required': False}}


class ZoneSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = Zone
        fields = '__all__'


class PersonZoneSerializer(AddAgencyMixin, serializers.ModelSerializer):
    class Meta:
        model = PersonZone
        fields = '__all__'
