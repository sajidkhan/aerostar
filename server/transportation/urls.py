from django.urls import include, re_path
from rest_framework.routers import DefaultRouter
from transportation.views import TransportationTypeViewSet, VehicleViewSet, ZoneViewSet, \
    PersonZoneViewSet

router = DefaultRouter()
router.register(r'transportation_types', TransportationTypeViewSet, basename='transportation_type')
router.register(r'vehicles', VehicleViewSet, basename='vehicle')
router.register(r'zones', ZoneViewSet, basename='zone')
router.register(r'person_zones', PersonZoneViewSet, basename='person_zones')

urlpatterns = [
    re_path('', include(router.urls)),
]
