from djangorestframework_camel_case.parser import CamelCaseJSONParser
from rest_framework import viewsets
from aerostar.models import UserType
from server.multipart_formencode_parser import MultipartFormencodeParser
from transportation.models import TransportationType, Vehicle, Zone, PersonZone
from transportation.serializers import TransportationTypeSerializer, VehicleSerializer, ZoneSerializer, PersonZoneSerializer
from aerostar.permissions import UserPermissionFactory, UserPermission
from functools import partial


class TransportationTypeViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER], UserPermission.NO_PERMISSIONS),
        UserPermission([UserType.ADMIN], ['GET', 'HEAD', 'OPTIONS', 'PUT', 'PATCH', 'POST']),
    ]),)
    serializer_class = TransportationTypeSerializer
    queryset = TransportationType.objects.all()

    def get_queryset(self, *args, **kwargs):
        return TransportationType.objects.filter(agency_id=self.request.user.agency_id)


class VehicleViewSet(viewsets.ModelViewSet):
    parser_classes = [MultipartFormencodeParser, CamelCaseJSONParser]
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.USER], UserPermission.NO_PERMISSIONS),
        UserPermission([UserType.ADMIN], ['GET', 'HEAD', 'OPTIONS', 'PUT', 'PATCH', 'POST']),
    ]),)
    serializer_class = VehicleSerializer
    queryset = Vehicle.objects.all()

    def get_queryset(self, *args, **kwargs):
        return Vehicle.objects.filter(agency_id=self.request.user.agency_id)


class ZoneViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.ADMIN, UserType.USER],
                       UserPermission.NO_PERMISSIONS),
    ]),)
    serializer_class = ZoneSerializer
    queryset = Zone.objects.all()

    def get_queryset(self, *args, **kwargs):
        return Zone.objects.filter(agency_id=self.request.user.agency_id)


class PersonZoneViewSet(viewsets.ModelViewSet):
    permission_classes = (partial(UserPermissionFactory, [
        UserPermission([UserType.COACH, UserType.TRANSPORTER, UserType.ADMIN, UserType.USER],
                       UserPermission.NO_PERMISSIONS),
    ]),)
    serializer_class = PersonZoneSerializer
    queryset = PersonZone.objects.all()

    # def get_queryset(self, *args, **kwargs):
    #     return PersonZone.objects.filter(zone__agency_id=self.request.user.agency_id)
