# All django apps that we did not create should go here
INSTALLED_APPS = [
    'corsheaders',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'djoser',
    'rest_framework',
    'rest_framework.authtoken',
    'safedelete',
    'simple_history',
    'djmoney',
    'django_filters',
    'prettyjson',
    'storages',
    'import_export',
]

# Our Custom Apps Go Here
MY_APPS = [
    'aerostar.apps.AerostarConfig',
    'schedules.apps.SchedulesConfig',
    'transportation.apps.TransportationConfig',
    'forms.apps.FormsConfig',
]

INSTALLED_APPS = INSTALLED_APPS + MY_APPS