from djangorestframework_camel_case.settings import api_settings
from djangorestframework_camel_case.util import underscoreize
from rest_framework import parsers
import json


class MultipartFormencodeParser(parsers.MultiPartParser):
    json_underscoreize = api_settings.JSON_UNDERSCOREIZE

    def parse(self, stream, media_type=None, parser_context=None):
        result = super().parse(
            stream,
            media_type=media_type,
            parser_context=parser_context
        )

        data = {}
        for key, value in result.data.items():
            key = underscoreize(key, **self.json_underscoreize)
            try:
                data[key] = json.loads(value)
            except Exception as e:
                data[key] = value

        data = underscoreize(data, **self.json_underscoreize)
        return parsers.DataAndFiles(data, result.files)