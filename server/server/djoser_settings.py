import environ

env = environ.Env(
    PRODUCTION=(bool, False),
    DEBUG=(bool, True),
    SECRET_KEY=(str, ''),
)

if not env('SECRET_KEY'):
    environ.Env.read_env()

WEB_DOMAIN = 'TestDomain' if env('DEBUG') else env('WEB_DOMAIN')

DJOSER = {
    'PASSWORD_RESET_CONFIRM_URL': 'reset_password/{uid}/{token}',
    'PASSWORD_CHANGED_EMAIL_CONFIRMATION': True,
    "PERMISSIONS":
        {
            "activation": ["rest_framework.permissions.IsAdminUser"],
            "password_reset": ["rest_framework.permissions.AllowAny"],
            "password_reset_confirm": ["rest_framework.permissions.AllowAny"],
            "set_password": ["rest_framework.permissions.IsAdminUser"],
            "username_reset": ["rest_framework.permissions.IsAdminUser"],
            "username_reset_confirm": ["rest_framework.permissions.IsAdminUser"],
            "set_username": ["rest_framework.permissions.IsAdminUser"],
            "user_create": ["rest_framework.permissions.IsAdminUser"],
            "user_delete": ["rest_framework.permissions.IsAdminUser"],
            "user": ["rest_framework.permissions.IsAdminUser"],
            "user_list": ["rest_framework.permissions.IsAdminUser"],
            "token_create": ["rest_framework.permissions.IsAdminUser"],
            "token_destroy": ["rest_framework.permissions.IsAuthenticated"],
        },
    "EMAIL":
        {
            'activation': 'aerostar.email.AerostarCreateUserPasswordResetEmail',
            'confirmation': 'djoser.email.ConfirmationEmail',
            'password_reset': 'aerostar.email.AerostarPasswordResetEmail',
            'password_changed_confirmation': 'aerostar.email.AerostarPasswordChangedConfirmationEmail',
            'username_changed_confirmation': 'djoser.email.UsernameChangedConfirmationEmail',
            'username_reset': 'djoser.email.UsernameResetEmail',
        },
    'DOMAIN': WEB_DOMAIN,
    'SITE_NAME': 'Aerostar',
}
