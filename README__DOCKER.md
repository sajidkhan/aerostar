# Aerostar Docker


## Project Requirements
- Docker and Docker-compose
- .env file 
- WSL2
- Docker Desktop

## Project Set up
* create docker-compose.yml file inside `\Aerostar-br_Docker\server\` directory
  - `cd \Aerostar-br_Docker\server\`
  - new file name `docker-compose.yml`
  - copy and paste following YML code into `docker-compose.yml`
```
version: "3.8"

services:
  db:
    image: postgres:14
    container_name: db
    ports:
      - 5432:5432
    volumes:
      - ./data/db:/var/lib/postgresql/data
      - ./postgres/pg_hba.conf:/usr/share/postgresql/14/pg_hba.conf
      - ./sql/aerostarDB.sql:/docker-entrypoint-initdb.d/aerostarDB.sql
    environment:
      - POSTGRES_DB=aerostar
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=scorpion
      - POSTGRES_HOST=127.0.0.1
  server:
    build: .
    container_name: server-drf
    restart: on-failure
    volumes:
      - .:/server
    ports:
      - "8000:8000"
    env_file:
      - ./server/.env
    depends_on:
      - db
  client:
    container_name: client
    build:
      context: ../client/
    ports:
      - 3000:3000
    volumes:
      - ../client/src:/client/src
    depends_on:
      - db
      - server

  pgadmin:
    container_name: pgadmin
    image: dpage/pgadmin4:latest
    environment:
      - PGADMIN_DEFAULT_EMAIL=admin@aerostar.com
      - PGADMIN_DEFAULT_PASSWORD=scorpion
    ports:
      - "5050:80"
    restart: always

```
* create Dockerfile file inside `\Aerostar-br_Docker\server\` directory
  - `cd \Aerostar-br_Docker\server\`
  - new file name `Dockerfile`
  - copy and paste following code into `Dockerfile`
```
FROM python:3.10
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

COPY . /server/
WORKDIR /server
RUN apt install --reinstall libpq-dev
RUN pip install -r requirements.txt
COPY . .

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod o+x /entrypoint.sh
EXPOSE 8000
ENTRYPOINT ["sh", "-c", "/entrypoint.sh"]
```
* create entrypoint.sh file inside `\Aerostar-br_Docker\server\` directory
  - `cd \Aerostar-br_Docker\server\`
  - new file name `entrypoint.sh`
  - copy and paste following code into `entrypoint.sh`
```
#!/bin/sh
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata aerostar/fixtures/user_types_fixtures aerostar/fixtures/location_fixtures
# python manage.py createaerostarsuperuser darlanebrown@gmail.com darla brown scorpion
python manage.py runserver 0.0.0.0:8000
```
* create .env file inside `\Aerostar-br_Docker\server\server\` directory
  - `cd \Aerostar-br_Docker\server\server\`
  - new file name `.env`
  - copy and paste following code into `.env`
```
DEBUG=True
SECRET_KEY=1234-1234
DB_NAME=aerostar
DB_HOST=db
DB_USER=postgres
DB_PASSWORD=scorpion
DB_PORT=5432
EMAIL_HOST_USER=
EMAIL_HOST_PASSWORD=
EMAIL_HOST=
EMAIL_PORT=
EMAIL_USE_TLS=
EMAIL_DOMAIN=
EC2_HOST=
S3_HOST=
SENTRY_DSN=
SESSION_COOKIE_DOMAIN=
WEB_DOMAIN=
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_STORAGE_BUCKET_NAME=
DEFAULT_FROM_EMAIL=
```
* create Dockerfile file inside `\Aerostar-br_Docker\client\` directory
  - `cd \Aerostar-br_Docker\client\`
  - new file name `client`
  - copy and paste following code into `client`
```
FROM node:18-alpine

WORKDIR /client
COPY package.json .
RUN yarn install
COPY . .
CMD ["yarn", "start"]
```
## How to run the Project
* `cd \Aerostar-br_Docker\server\`
* `docker-compose up -d  ` - it'll up and run all Docker services, volumes, networks and containers
* `docker-compose ps   ` - to see the running services
~~~shell
   Name                 Command               State                       Ports                    
---------------------------------------------------------------------------------------------------
client       docker-entrypoint.sh yarn  ...   Up      0.0.0.0:3000->3000/tcp,:::3000->3000/tcp     
db           docker-entrypoint.sh postgres    Up      0.0.0.0:5432->5432/tcp,:::5432->5432/tcp     
pgadmin      /entrypoint.sh                   Up      443/tcp, 0.0.0.0:5050->80/tcp,:::5050->80/tcp
server-drf   sh -c /entrypoint.sh             Up      0.0.0.0:8000->8000/tcp,:::8000->8000/tcp  
~~~
* `docker-compose stop   ` - to stop all the running services 
* `docker-compose start   ` - to start all the services 
* `docker-compose restart   ` - to restart all the  services 
* `docker-compose down   ` - to destory all the  services,networks and containers
* `docker-compose down -v  ` - to destory all the  services,networks and containers including volumes
* `docker-compose up -d --build   ` - to rebuild all images.