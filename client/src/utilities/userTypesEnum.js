export const UserTypesEnum = {
  PARTICIPANT: 'Participant',
  COACH: 'Coach',
  USER: 'User',
  ADMIN: 'Admin',
  TRANSPORTER: 'Transporter',
};
