export const UserTypeValues = {
  PARTICIPANT: 1,
  COACH: 2,
  USER: 3,
  ADMIN: 4,
  TRANSPORTER: 5,
};
