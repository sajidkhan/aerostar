export function formatAddress(address: Address) {
  if (!address) return ' ';
  return `${address.address} ${address.addressTwo || ''} ${address.city}, ${
    address.state
  } ${address.zip}`;
}
