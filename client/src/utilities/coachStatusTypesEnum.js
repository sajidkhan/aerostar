export const CoachStatusTypesEnum = {
  SCHEDULED: 'scheduled',
  SCHEDULED_OFF: 'scheduled_off',
  UNSCHEDULED: 'unscheduled',
  CALLED_OUT: 'called_out',
};
