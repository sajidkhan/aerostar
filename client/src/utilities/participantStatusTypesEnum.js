export const ParticipantStatusTypesEnum = {
  SCHEDULED: 'scheduled',
  OFF: 'off',
  CALLED_OUT: 'called_out',
  NO_SHOW: 'no_show',
  DECLINED_SERVICES: 'declined_services',
};
