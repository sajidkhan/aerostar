export const DocumentTypesEnum = {
  BACKGROUND: 'Background',
  IMMUNIZATION: 'Immunization',
  VEHICLE: 'Vehicle',
  LICENSE: 'License',
};
