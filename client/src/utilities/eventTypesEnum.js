export const EventTypesEnum = {
  PICKUP: 1,
  CHECK_IN: 2,
  ARRIVE: 3,
  CALLED_OUT: 4,
  NO_SHOW: 5,
  DEPART: 7,
  DONE: 8,
};
