// @flow
import { createSlice } from '@reduxjs/toolkit';
import { defaultReducers } from './defaultReducers';
import { DefaultRequests } from './defaultRequests';

export interface Document {
  id: number;
  sThreeLocation: string;
  vehicle?: number;
  user: number;
  type: string;
}

export interface DocumentState {
  loading: boolean;
  error: string;
  list: Document[];
  byId: {};
}

const initialState: DocumentState = {
  loading: false,
  error: '',
  list: [],
  byId: {},
};

const documents = createSlice({
  name: 'documents',
  initialState,
  reducers: defaultReducers,
});

export const documentReducer = documents.reducer;
export const documentActions = new DefaultRequests(
  documents.actions,
  'aerostar/documents'
);
