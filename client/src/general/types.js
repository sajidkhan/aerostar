// @flow
import type { ScheduleState } from '../slices/scheduleSlice';
import type { CoachState } from '../slices/coachesSlice';
import type { ParticipantState } from '../slices/participantSlice';
import type { AuthState } from '../slices/authSlice';
import type { LocationState } from '../slices/locationSlice';
import type { SkillState } from '../slices/skillSlice';
import type { SearchState } from '../slices/searchSlice';
import type { UserState } from '../slices/userSlice';
import type { ProgramTransferState } from '../slices/transferProgramSlice';
import type { ProgramState } from '../slices/programSlice';
import type { ScheduleTypeState } from '../slices/scheduleTypeSlice';
import type { ParticipantHistoryState } from '../slices/participantHistorySlice';
import type { EncounterTypeState } from '../slices/encounterTypeSlice';
import type { EventTypeState } from '../slices/eventTypeSlice';
import type { GroupState } from '../slices/groupSlice';
import type { SurveyState } from '../slices/surveySlice';

export type State = {
  search: SearchState,
  participantUsers: ParticipantState,
  participantHistory: ParticipantHistoryState,
  coachUsers: CoachState,
  locations: LocationState,
  skills: SkillState,
  programs: ProgramState,
  programTransfer: ProgramTransferState,
  schedule: ScheduleState,
  schedule_types: ScheduleTypeState,
  isFetching?: boolean,
  auth: AuthState,
  users: UserState,
  coaches: CoachState,
  participants: ParticipantState,
  encounter_type: EncounterTypeState,
  event_types: EventTypeState,
  groups: GroupState,
  surveys: SurveyState,
};
