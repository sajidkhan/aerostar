import React from 'react';
import { render } from '@testing-library/react';
import UserPresenter from '../presenters/UserPresenter';
import type { CurrentUser } from '../slices/authSlice';
import { UserTypesEnum } from '../utilities/userTypesEnum';
const context = describe;

describe('UserPresenter', () => {
  context('when CurrentUser has a firstName and lastName', () => {
    let currentUser: CurrentUser = { firstName: 'first', lastName: 'last' };
    let userPresenter = new UserPresenter(currentUser);
    describe('.firstLastNames()', () => {
      test('firstLastNames should be equal to first last', () => {
        expect(userPresenter.firstLastNames()).toBe('first last');
      });
    });
    describe('.initials()', () => {
      test('initials should be equal to FL', () => {
        expect(userPresenter.initials()).toBe('FL');
      });
    });
  });
  context('when CurrentUser has no firstName nor lastName', () => {
    let userPresenter = new UserPresenter({});
    describe('.firstLastNames()', () => {
      test('firstLastNames should be the empty string', () => {
        expect(userPresenter.firstLastNames()).toBe('');
      });
    });
    describe('.initials()', () => {
      test('initials should be the empty string', () => {
        expect(userPresenter.initials()).toBe('');
      });
    });
  });
  context('when CurrentUser has COACH userType', () => {
    let currentUser: CurrentUser = { userType: UserTypesEnum.COACH };
    let userPresenter = new UserPresenter(currentUser);
    describe('.isCoachUserType()', () => {
      test('isCoachUserType should be Truthy', () => {
        expect(userPresenter.isCoachUserType()).toBeTruthy();
      });
    });
  });
  context('when CurrentUser does not have COACH userType', () => {
    let currentUser: CurrentUser = { userType: UserTypesEnum.USER };
    let userPresenter = new UserPresenter(currentUser);
    describe('.isCoachUserType()', () => {
      test('isCoachUserType should be Falsy', () => {
        expect(userPresenter.isCoachUserType()).toBeFalsy();
      });
    });
  });
});
