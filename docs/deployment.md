# Deployment

We are hosting the application in AWS, using:
- RDS - hosting the Postgres DB
- ElasticBeanstalk - managing the EC2 environments
- S3 and CloudFront - hosting the Client React app

### AWS Configuration files
#### Credentials
- Add 'aerostar' credentials to your .aws/credentials file from zoho
 e.g.
```
[aerostar]
aws_access_key_id = YOUR_ID
aws _secret_access_key = YOUR_KEY
```
- Add 'aerostar' entry to .aws/config e.g.
```
[profile aerostar]
output-json
region = us-west-2
```

### Elastic Beanstalk
There should be a `server/.elasticbeanstalk` and `server/.ebextensions` folders
with the environment and start up scripts.
## Deploying to Staging

- If `.elasticbeanstalk` is missing, run `eb init --profile aerostar`
- If `.ebextensions` is missing, contact your lead/manager.

### Server
- `eb deploy --profile aerostar AerostarDevelopment-env`

**note:** If AWS complains, it's most likely because it cannot read your region so,

- `eb deploy --profile aerostar AerostarDevelopment-env` --region=us-west-2

### Client
- `yarn build:staging`
- `yarn deploy:staging`

## Deploying to Production
### Server
- `eb deploy --profile aerostar Aerostarproduction-env-1`

### Client
- `yarn build:production`
- `yarn deploy:production`
