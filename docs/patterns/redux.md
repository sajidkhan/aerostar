# Redux Patterns
We use the recommended Redux Toolkit pattern (https://redux.js.org/introduction/getting-started), but we also do the following:

## Default Reducer
(/client/src/slices/defaultReducers.js)

You can think of this reducer as the "Helper Reducer" that you can use to make
some boilerplate easier. Some of the things, it can do is:

* A way to normalize all of the state objects so that they behave similar.
* A way to parse API response payload and update the loading state.

### State
To provide this, the helper reducer normalizes the state, so if you use it, we
we expect the state object to have the following properties:

```javascript
{
  loading: false,
  error: '',
  list: [],
  byId: {},
};
```

Let's go into more detail about what some of these properties contain:
* `list: []` -> All of the individual Objects. Useful if you want to loop through
  each object.
* `byId: {}` -> The `key` will be the primary key of the Object and the `value` will
   be the actual Object. This is useful if you want to find an Object and have it's ID.

**Internal note**: I believe we can get rid of the `list[]` property, since we can get
those values by doing `Object.values(state.byId)`, but it would require some refactoring and testing.

### Example
```javascript
export interface Program {
  id: number;
  name: string;
}

export interface ProgramState {
  loading: boolean;
  error: string;
  list: Program[];
  byId: {};
}
const program = createSlice({
  name: 'programs',
  initialState,
  reducers: {
    getStart: defaultReducers.getStart,
    getSuccess: defaultReducers.getSuccess,
    getFailure: defaultReducers.getFailure,
    normalizeList: defaultReducers.normalizeList,
    add: defaultReducers.add,
    update: defaultReducers.update,
  },
});
```
