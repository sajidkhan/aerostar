# DB Patterns

## Django Model
While we are using Django's built in Models for the bulk of our work, there are a 
couple of libraries that act as middleware for how the data is read/saved. 

## Django Safe Delete
https://github.com/makinacorpus/django-safedelete

### Deleting objects
We are using this to prevent record from actually being deleted from the db.
When you run `delete DjangoModel`, the ORM will normally convert that to a 
`DELETE FROM table ...` but this library hijacks that and instead:

* Creates a `deleted` boolean property on the Model.
* On delete, will send a `UPDATE table SET delete = {timestamp} ....`

so it ensures that we never lose data.

### Fetching deleted objects
To get the deleted objects, you can do:
```
    Model.objects_all.filter(...)
```

if you do the `Model.objects` (without the `_all`), the library will filter
the deleted objects for you `SELECT ... FROM table WHERE deleted = NULL`

## Django Simple History
https://github.com/jazzband/django-simple-history

This library also modifies the Model, so that whenever a Create/Update/Delete
occurs, it:

* Creates a `table_historical` table.
* Inserts into this table the _current_ model data AND the user that modified this
data.
* Updates the existing row with the new data.

There are ways to query this historical table and to create a diff between all
the changes. 

```
# Example from their docs
p = Poll.objects.create(question="what's up?")
p.question = "what's up, man?"
p.save()

new_record, old_record = p.history.all()
delta = new_record.diff_against(old_record)
for change in delta.changes:
    print("{} changed from {} to {}".format(change.field, change.old, change.new))
```