# Development Set Up
Aerostar uses:
- Django with Django Rest Framework
- React with Redux
- Postgres

### Server
#### Initialize the database 

To initialize the database, you'll need to have postgreSQL installed locally. Once you have that installed, you can run:
```shell
$ psql
```

In psql shell, run:
```postgresql
CREATE DATABASE aerostar;
```

###### IMPORTANT: All of the commands should be performed within the `<PROJ_ROOT>/server` directory.

#### Initialize virtual environment — [virtualenv](https://virtualenv.pypa.io/en/latest/installation.html)

Initializing the virtual environment assumes you already have virtualenv installed. In the `server` directory, run:
```shell
$ virtualenv venv
```

#### Activate virtual environment

```shell
$ source venv/bin/activate
```
This will drop you in a virtual environment and keep dependancies in isolation.

#### Install python modules
To install dependancies run:
```shell
$ pip install -r requirements.txt
```
###### NOTE: If you aren't within the virtualenvironment (you didn't activate the virtual environment), this command will install depandancies into your global python package directories. 

#### Create environment variables
You need a `.env` file in the `<PROJ_ROOT>/server/server` directory. You can create one or you can copy the `sample.env`.

###### NOTE: You may have to use another team member's environment variables if you run into errors.

```shell
$ cp sample.env server/.env
```

#### Migrate the database
To run migrations, run:
```shell
$ python manage.py migrate
```

#### Load initial data
To load the database with fixtures, run:
```shell
$ python manage.py loaddata aerostar/fixtures/user_types_fixtures aerostar/fixtures/location_fixtures schedules/fixtures/event_types_fixture
```
To create your superuser, run:
```shell
$ python manage.py createaerostarsuperuser <email> <firstname> <lastname> <password>
```

### Client
- `yarn install`

## Running the Project


**NOTE:** Notice that we are running on `127.0.0.1` and NOT `localhost`. If you
don't use the local IP address, the React will display, but it will be unable
to speak to the server.

### Server
- `python manage.py runserver`
- View at [http://127.0.0.1:8000](http://127.0.0.1:8000)

### Client
- `yarn start`
- View at [http://127.0.0.1:3000](http://127.0.0.1:3000)

## Running Tests
- `python manage.py test --snapshot-update`
