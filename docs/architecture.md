# Archictecture

For the full **original** scope, see https://docs.google.com/document/d/1AfC86g2cun-l21AGenNr_v-gYZRyzIiu17JzI56dLBc/.

The main functionality and architecture hasn't deviated too much from that, with
the exception of Transportation Type, which is changing due to the way agencies
changed because of COVID.

## Database
### ERD
Here is how the DB is structured (and by relation the Django Models).
![ERD](./ERD.png)

### Interacting with DB
See [DB Pattern](./patterns/db.md) for more details on how to query/update
the database.


## API
For our API, we are using a mix of Django and Django Rest Framework to 
facilitate a lot of the boilerplate for us.

* Django (https://www.djangoproject.com/)
* DRF (https://www.django-rest-framework.org/)

## Structure
Our API is structured using the follwing 3 Django Projects:
* `server` -> The 'main' application that co-ordinates everything.
  This sets up the middleware, login, routes to other Django projects and server settings. 
* `aerostar` -> Contains all of the CRUD functionality for the app
* `schedules` -> Anything to do with scheduling participants or loading
  the schedules for every type of user.
* `questionnaires` -> Functionality for recording the questionnaire data.
* `transportation` -> We are re-working Transportation, so this might be obsolete.


### Login
We use a mix of Django's built-in authentication with https://djoser.readthedocs.io/en/latest/introduction.html for password reset functionality.


## Client
We are using:
- React 
- Redux - with the recommended Redux Toolkit (https://redux.js.org/introduction/getting-started)
- Ant Design (https://ant.design/components/overview/) for our UI library.

