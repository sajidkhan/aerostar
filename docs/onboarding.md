# Onboarding Agency
This is the Standard Operating Procedure for adding a new Agency to the system.

1. Create a new Agency via `api/v1/aerostar/agencies/`.
    This will create the Agency, two TransportationTypes (FMS and Standard)
    and 3 ScheduleTypes (start, event, end).
    **If you use the Django Admin to do so, you will have to create all of this yourself.**
2. Upload the Excel to Spreasheets and repeat Step 3, for each sheet that we need to upload.

3. Navigate to the Location/Skills/etc sheet.
   a. Add a new column called Agency and fill out the Agency ID.
      If you need to replace one of the values with an ID, so do here (Program name => Program ID).
   b. Rename the header to all lower case
   c. Go to `File -> Download -> csv`, which will download only that sheet.
3. Navigate to `api/admin/aerostar/` and find the table you want to update to
   a. Click on the `import` button on the top right.
   b. Select the CSV file and click Submit.
   c. A preview of the rows that will be uploaded will be shown - make sure the columns are filled out. If there are not, make sure the column names are **exactly the same (lowecased)**

   * If you find an error due to a name being too long, you'll have to update the CSV and truncate it.
   * You might also have to rename some of the columns if the header name differs from the column name.
   * There might be some utf-8 characters that are not supported (table is in ASCII), so we'll have to remove those =(
4. Confirm Import 