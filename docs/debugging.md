# Debugging

## Server
## pdb
I've had mixed experience using the PythonPDB `import pdb; pdb.set_trace()`.
It stops the program and you can interact with the state, but when I attempt to give Django control again, the debugger seems to never exit and return
execution to Django.
So while it works, you _might_ need to kill the server and restart it for every request.

### Logs
Logs are currently being written to the root repo directory, using a rolling file pattern. You should find them under `repo_root/django.log`. 

If you open that file, you'll be able to see all of the requests and SQL commands the
ORM is performing. You can use this to make sure the correct queries are being run.

#### Production
**TODO: Write the Cloudwatch instructions**

## Client
- Good old `debug` statements work.
- Inspecting requests via the `Network` tab in developer tools.

**Note**: When on the debugger and you want to see the values of the `state`, you can't just
`console.log(state)` because it's a proxy object from Redux. What you need to do is:
```
JSON.parse(JSON.stringify(state));
```

## Database
### Production
Because our product is HiPPA compliant (and it's good security practice), we have the database completely locked out from public access.

This makes it extremely difficult to debug production data issues, since we have to:
* SSH into the EC2 instance
* Connect to the DB using postgres cli

We have managed to improve this workflow by creating a bastion machine (who's sole purpose is to act as proxy between our local computer and the production DB).
This way, we:

  * Create a tunnel between our computer and the bastion instance. 
  * The bastion has access to the database. 
  * Because we have a tunnel, our local GUI client now has access to the production database.
 
Read more here: https://aws.amazon.com/premiumsupport/knowledge-center/rds-connect-ec2-bastion-host/

** ------------------------------------------------------------------------------------*

**NOTE: FOR SECURITY CONCERNS DO NOT SAVE ANY OF THE PRODUCTION CREDENTIALS LOCALLY!!!**

** ------------------------------------------------------------------------------------*

### How to connect
To connect to the bastion machine here are the instructions:

  * AWS Link: https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#InstanceDetails:instanceId=i-0c68c488ff11406fe
  * IP: 54.70.18.57
  * Pem Key: In vault in the Aerostar folder

Initial Configuration:


** ------------------------------------------------------------------------------------*

**NOTE: FOR SECURITY CONCERNS DO NOT SAVE ANY OF THE PRODUCTION CREDENTIALS LOCALLY!!!**

** ------------------------------------------------------------------------------------*

1. Be sure to add your public IP to the SG group for the Bastion host in AWS. 
2. Once this is done you will need to configure your client to use the host as a tunnel, with the pem key as the authentication as mentioned in the article. 

Using DB Beaver, you can then:
1. Use the SSH connection
![image](https://user-images.githubusercontent.com/3130883/111847722-a3ec3300-88c6-11eb-9268-93a499e05ae0.png)

2. Connect to the prod db.
![CleanShot 2021-03-19 at 15 20 39](https://user-images.githubusercontent.com/3130883/111847905-1230f580-88c7-11eb-8f4a-34a5bf590849.png)
